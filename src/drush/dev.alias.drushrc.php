<?php

$options = array(
    'uri' => 'development.arena.buzzaka.net',
    'remote-host' => '37.187.57.175',
    'remote-user' => 'webdev',
    'root' => '/home/webdev/arena/site/development/htdocs/',
    'path-aliases' => array(
        '%files' => 'sites/default/files',
        '%private_files' => '../arena-private-files/',
    )
);
