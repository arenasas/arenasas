<header class="l-header" role="banner">
    <div class="l-header-inner clearfix">
        <div class="l-branding">
            <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"
                   class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/></a>
            <?php endif; ?>
        </div>

        <div class="header-menus">
            <div class="burger-icon">
                <div class="bar top-bun"></div>
                <div class="bar meat"></div>
                <div class="bar bot-bun"></div>
            </div>
            <?php
                print render($page['header']);
            ?>
        </div>
    </div>
</header>
