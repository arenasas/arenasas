<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes; ?> suppliers-infos-details"<?php print $attributes; ?>>
    <?php if (!$label_hidden): ?>
        <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?></div>
    <?php endif; ?>
    <div class="field-items"<?php print $content_attributes; ?>>
        <?php foreach ($items as $delta => $item): ?>
            <div class="row field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
                <?php
                $form = drupal_get_form('_main_supplier_general_information_form');
                $item = json_decode($item['#markup'], TRUE);
                ?>
                <div class="table-wrapper col-md-12">
                    <div class="table-title">General informations</div>
                    <table>
                        <?php if (!empty($item['type_of_business']) && !empty($form['fieldset_1']['type_of_business'])) : ?>
                        <tr>
                            <td>
                                <?php echo $form['fieldset_1']['#title'] ?>
                            </td>
                            <td>
                                <?php echo $form['fieldset_1']['type_of_business']['#options'][reset($item['type_of_business'])] ?>
                                <?php if (!empty($item['type_of_business_precise'])) : ?>
                                    (<?php echo $item['type_of_business_precise']; ?>)
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php if (!empty($item['ownership']) && !empty($form['fieldset_2']['ownership'])) : ?>
                        <tr>
                            <td>
                                <?php echo $form['fieldset_2']['#title'] ?>
                            </td>
                            <td>
                                <?php echo $form['fieldset_2']['ownership']['#options'][reset($item['ownership'])] ?>
                                <?php if (!empty($item['ownership_precise'])) : ?>
                                    (<?php echo $item['ownership_precise']; ?>)
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php if (!empty($item['nationality_of_ownership']) && !empty($form['fieldset_2']['nationality_of_ownership'])) : ?>
                        <tr>
                            <td>
                                <?php echo $form['fieldset_2']['nationality_of_ownership']['#title'] ?>
                            </td>
                            <td>
                                <?php echo $form['fieldset_2']['nationality_of_ownership']['#options'][$item['nationality_of_ownership']] ?>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php if (!empty($item['nationality_of_ownership']) && !empty($form['fieldset_2']['nationality_of_ownership'])) : ?>
                        <tr>
                            <td>
                                <?php echo $form['fieldset_3']['#title'] ?>
                            </td>
                            <td>
                                <?php
                                foreach ($item['product_subfamilies'] as $key => $category) :
                                    foreach ($category as $subfamily) {
                                        $subfamily = taxonomy_term_load($subfamily);
                                        echo $subfamily->name . ', ';
                                    }
                                endforeach;
                                ?>
                            </td>
                        </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
