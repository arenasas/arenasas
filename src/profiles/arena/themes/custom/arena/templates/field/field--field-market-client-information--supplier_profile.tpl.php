<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes; ?> suppliers-infos-details"<?php print $attributes; ?>>
    <?php if (!$label_hidden): ?>
        <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?></div>
    <?php endif; ?>
    <div class="field-items"<?php print $content_attributes; ?>>
        <?php $currency = MainSupplierAPI::getCurrency($element['#object']->nid); ?>
        <?php foreach ($items as $delta => $item): ?>
            <div
                class="row field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
                <?php
                $form = drupal_get_form('_main_supplier_market_client_information_form');
                $item = json_decode($item['#markup'], TRUE);

                if (!empty($item['country'])) : ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_1']['#title']; ?></div>
                        <?php
						// Boucle permettant l'affichage de tableaux sur plusieurs lignes. Le fonctionnement est similaire à celui d'une pagination classique.
						$items_per_line = 3; // On définie le nombre de colonnes par ligne que l'on souhaite (ne compte pas la colonne de titres)
                        $nb_items = count($item['country']); // Nombre de colonnes total à afficher
                        $nb_tabs = ceil($nb_items / $items_per_line); // Calcul du nombre de lignes nécessaires en fonction des deux paramètres précédents
						// ceil prend l'arrondi au nombre supérieur du résultat de la division.

                        $cpt = 1; // Initialisation d'un compteur
                        for ($j = 1; $j <= $nb_tabs; $j++) : // On boucle sur le nombre de lignes nécessaires calculé précédemment
							$flag = $j * $items_per_line;
							// $cpt et $flag sont des marqueurs qui permettent de connaitre l'interval courant des colonnes à afficher (ex. 1 -3, 4 - 6, 7 - 9).
						?>
							<table<?php echo ($j == 1) ? ' class="first-table" ' : '' ?>>
                                <tbody>
                                    <tr> <!-- COUNTRY LINE -->
                                        <td>
                                            <?php
                                            if (!empty($form['fieldset_1']['country'][1]['country']['#title'])) :
                                                echo $form['fieldset_1']['country'][1]['country']['#title'];
                                            endif;
                                            ?>
                                        </td>
                                        <?php for ($l = $cpt; $l <= $flag; $l++) {
											// Ici on boucle sur l'interval courant pour afficher le nombre de colonnes désiré
											// A effectuer pour chaque valeur du tableau ?>
											<?php if (!empty($item['country'][$l])) { ?>
												<td>
													<?php echo $form['fieldset_1']['country'][$l]['country']['#options'][$item['country'][$l]['country']]; ?>
												</td>
											<?php } else { ?>
												<td class="empty-cell"></td>
											<?php } ?>
										<?php } ?>
                                    </tr>
                                    <tr> <!-- TURNOVER PER ALLY LINE -->
                                        <td>Turnover per ally <?php echo '('.$currency.')'; ?></td>
										<?php $totals = array(); ?>
                                        <?php for ($l = $cpt; $l <= $flag; $l++) { ?>
											<?php if (!empty($item['country'][$l])) { ?>
												<td>
													<table>
														<tbody>
															<?php if (isset($item['country'][$l]['turnover_per_ally'])) : ?>
																<?php foreach ($item['country'][$l]['turnover_per_ally'] as $key_tpa => $tpa) : ?>
																<tr>
																	<td><?php echo $form['fieldset_1']['country'][$l]['turnover_per_ally'][1]['allies']['#options'][$tpa['allies']]; ?></td>
																	<td><?php echo $tpa['turnover']; $totals[$l] = isset($totals[$l]) ? $totals[$l] + $tpa['turnover'] : $tpa['turnover'] ?></td>
																</tr>
																<?php endforeach; ?>
															<?php endif; ?>
														</tbody>
													</table>
												</td>
											<?php } else { ?>
											    <?php $totals[$l] = 'empty'; ?>
												<td class="empty-cell"></td>
											<?php } ?>
										<?php } ?>
                                    </tr>
									<tr>
										<td class="total-cell"><?php print t('Total'); echo ' ('.$currency.')' ?></td>
										<?php foreach ($totals as $total) : ?>
										  	<?php if ($total == 'empty') : ?>
												<td class="empty-cell"></td>
											<?php else : ?>
										  		<td class="total-cell total-cell-right"><?php echo $total; ?></td>
											<?php endif; ?>
										<?php endforeach; ?>
									</tr>


                                </tbody>
                            </table>
                        <?php
						$cpt = $cpt + 3;
						endfor;
						?>
                    </div>
                <?php endif;
                if (!empty($item['product_category'])) : ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_2']['#title']; ?></div>
						<?php
						$items_per_line = 3;
                        $nb_items = count($item['product_category']);
                        $nb_tabs = ceil($nb_items / $items_per_line);
                        $cpt = 1;
                        for ($j = 1; $j <= $nb_tabs; $j++) :
							$flag = $j * $items_per_line;
						?>
							<table<?php echo ($j == 1) ? ' class="first-table" ' : '' ?>>
								<tbody>
									<tr> <!-- Product subfamilies LINE -->
										<td>
											<?php
											if (!empty($form['fieldset_2']['product_category'][1]['product_options']['#title'])) :
												echo $form['fieldset_2']['product_category'][1]['product_options']['#title'];
											endif;
											?>
										</td>
										<?php for ($l = $cpt; $l <= $flag; $l++) {
											if (!empty($item['product_category'][$l])) { ?>
												<td>
													<?php echo $form['fieldset_2']['product_category'][$l]['product_options']['#options'][$item['product_category'][$l]['product_options']]; ?>
												</td>
											<?php } else { ?>
												<td class="empty-cell"></td>
											<?php } ?>
										<?php } ?>
									</tr>
									<tr> <!-- Turnover percentage LINE -->
										<td>
											<?php
											if (!empty($form['fieldset_2']['product_category'][1]['turnover_percentage']['#title'])) :
												echo $form['fieldset_2']['product_category'][1]['turnover_percentage']['#title'].' (%)';
											endif;
											?>
										</td>
										<?php for ($l = $cpt; $l <= $flag; $l++) {
											if (!empty($item['product_category'][$l])) { ?>
												<td>
													<?php echo $item['product_category'][$l]['turnover_percentage']; ?>
												</td>
											<?php } else { ?>
												<td class="empty-cell"></td>
											<?php } ?>
										<?php } ?>
									</tr>
									<tr> <!-- Turnover LINE -->
										<td>
											<?php
											if (!empty($form['fieldset_2']['product_category'][1]['turnover']['#title'])) :
												echo $form['fieldset_2']['product_category'][1]['turnover']['#title'].' ('.$currency.')';
											endif;
											?>
										</td>
										<?php for ($l = $cpt; $l <= $flag; $l++) {
											if (!empty($item['product_category'][$l])) { ?>
												<td>
													<?php echo $item['product_category'][$l]['turnover']; ?>
												</td>
											<?php } else { ?>
												<td class="empty-cell"></td>
											<?php } ?>
										<?php } ?>
									</tr>
									<tr> <!-- Main brands LINE -->
										<td>
											<?php
											if (!empty($form['fieldset_2']['product_category'][1]['main_brands']['#title'])) :
												echo $form['fieldset_2']['product_category'][1]['main_brands']['#title'];
											endif;
											?>
										</td>
										<?php for ($l = $cpt; $l <= $flag; $l++) : ?>
											<?php if (!empty($item['product_category'][$l])) : ?>
												<td>
													<?php echo $item['product_category'][$l]['main_brands']; ?>
												</td>
											<?php else : ?>
												<td class="empty-cell"></td>
											<?php endif; ?>
										<?php endfor; ?>
									</tr>
								</tbody>
							</table>
						<?php endfor; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
