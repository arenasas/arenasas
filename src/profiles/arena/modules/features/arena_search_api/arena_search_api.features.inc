<?php
/**
 * @file
 * arena_search_api.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function arena_search_api_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function arena_search_api_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function arena_search_api_image_default_styles() {
  $styles = array();

  // Exported image style: product_teaser.
  $styles['product_teaser'] = array(
    'label' => 'Product teaser',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 230,
          'height' => 180,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: supplier_teaser.
  $styles['supplier_teaser'] = array(
    'label' => 'Supplier teaser',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 130,
          'height' => 115,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_default_search_api_index().
 */
function arena_search_api_default_search_api_index() {
  $items = array();
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Product index",
    "machine_name" : "default_node_index",
    "description" : "An automatically created search index for indexing node data. Might be configured to specific needs.",
    "server" : "product_server",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : {
            "default" : "0",
            "bundles" : { "product" : "product", "supplier_profile" : "supplier_profile" }
          }
        },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "field_product_nomenclature:parent" : "field_product_nomenclature:parent",
              "field_product_nomenclature:parents_all" : "field_product_nomenclature:parents_all",
              "field_product_subfamilies:parent" : "field_product_subfamilies:parent",
              "field_product_subfamilies:parents_all" : "field_product_subfamilies:parents_all"
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_extra_field_alter_settings" : { "status" : 1, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true, "field_company_name" : true, "body:value" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true, "body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : { "title_field" : true, "field_company_name" : true, "body:value" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true, "body:value" : true },
            "spaces" : "[^\\\\p{L}\\\\p{N}]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      },
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_arena_supplier" : { "type" : "boolean" },
        "field_company_name" : { "type" : "string" },
        "field_establishment_date" : { "type" : "integer" },
        "field_extra_zone_arena" : { "type" : "string" },
        "field_moderation_status" : { "type" : "string" },
        "field_product_country" : { "type" : "string" },
        "field_product_fob_price" : { "type" : "decimal" },
        "field_product_id" : { "type" : "string" },
        "field_product_moq_min_order_qty" : { "type" : "integer" },
        "field_product_nomenclature" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_product_nomenclature:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_nomenclature:parent" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_nomenclature:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_nomenclature:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_signboard" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_product_signboard:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_product_signboard:parent" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_signboard:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_signboard:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_subfamilies" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_product_subfamilies:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_subfamilies:parent" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_subfamilies:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_subfamilies:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "promote" : { "type" : "boolean" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "integer" },
        "sticky" : { "type" : "boolean" },
        "title_field" : { "type" : "text", "boost" : "5.0" },
        "type" : { "type" : "string" }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function arena_search_api_default_search_api_server() {
  $items = array();
  $items['product_server'] = entity_import('search_api_server', '{
    "name" : "DB server",
    "machine_name" : "product_server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "2",
      "partial_matches" : 0,
      "indexes" : { "default_node_index" : {
          "type" : {
            "table" : "search_api_db_default_node_index",
            "column" : "type",
            "type" : "string",
            "boost" : "1.0"
          },
          "promote" : {
            "table" : "search_api_db_default_node_index",
            "column" : "promote",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "sticky" : {
            "table" : "search_api_db_default_node_index",
            "column" : "sticky",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_default_node_index",
            "column" : "created",
            "type" : "date",
            "boost" : "1.0"
          },
          "changed" : {
            "table" : "search_api_db_default_node_index",
            "column" : "changed",
            "type" : "date",
            "boost" : "1.0"
          },
          "author" : {
            "table" : "search_api_db_default_node_index",
            "column" : "author",
            "type" : "integer",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_default_node_index",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "body:value" : {
            "table" : "search_api_db_default_node_index_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product_nomenclature" : {
            "table" : "search_api_db_default_node_index_field_product_nomenclature",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_signboard" : {
            "table" : "search_api_db_default_node_index_field_product_signboard",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_signboard:tid" : {
            "table" : "search_api_db_default_node_index_field_product_signboard_tid",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_signboard:name" : {
            "table" : "search_api_db_default_node_index_field_product_signboard_name",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          },
          "field_product_signboard:parent" : {
            "table" : "search_api_db_default_node_index_field_product_signboard_paren",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product_signboard:parents_all" : {
            "table" : "search_api_db_default_node_index_field_product_signboard_par_1",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product_nomenclature:tid" : {
            "table" : "search_api_db_default_node_index_field_product_nomenclature_ti",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_nomenclature:name" : {
            "table" : "search_api_db_default_node_index_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product_nomenclature:parent" : {
            "table" : "search_api_db_default_node_index_field_product_nomenclature_pa",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product_nomenclature:parents_all" : {
            "table" : "search_api_db_default_node_index_field_product_nomenclature__1",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_default_node_index",
            "column" : "status",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product_fob_price" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_product_fob_price",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_product_moq_min_order_qty" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_product_moq_min_order_qty",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product_id" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_product_id",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product_country" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_product_country",
            "type" : "string",
            "boost" : "1.0"
          },
          "title_field" : {
            "table" : "search_api_db_default_node_index_text",
            "type" : "text",
            "boost" : "5.0"
          },
          "field_company_name" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_company_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_establishment_date" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_establishment_date",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_extra_zone_arena" : {
            "table" : "search_api_db_default_node_index",
            "type" : "string",
            "boost" : "1.0",
            "column" : "field_extra_zone_arena"
          },
          "field_product_subfamilies" : {
            "table" : "search_api_db_default_node_index_field_product_subfamilies",
            "column" : "field_product_subfamilies",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_subfamilies:tid" : {
            "table" : "search_api_db_default_node_index_field_product_subfamilies_tid",
            "column" : "field_product_subfamilies_tid",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_subfamilies:name" : {
            "table" : "search_api_db_default_node_index_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product_subfamilies:parent" : {
            "table" : "search_api_db_default_node_index_field_product_subfamilies_par",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product_subfamilies:parents_all" : {
            "table" : "search_api_db_default_node_index_field_product_subfamilies_p_1",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_moderation_status" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_moderation_status",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_arena_supplier" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_arena_supplier",
            "type" : "boolean",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1"
  }');
  return $items;
}
