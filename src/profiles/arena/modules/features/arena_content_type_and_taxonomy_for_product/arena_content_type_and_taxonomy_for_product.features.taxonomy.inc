<?php
/**
 * @file
 * arena_content_type_and_taxonomy_for_product.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function arena_content_type_and_taxonomy_for_product_taxonomy_default_vocabularies() {
  return array(
    'product_nomenclature' => array(
      'name' => 'Product nomenclature',
      'machine_name' => 'product_nomenclature',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
    'product_quality_marks' => array(
      'name' => 'Product quality marks',
      'machine_name' => 'product_quality_marks',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
    'signboard' => array(
      'name' => 'Retailer',
      'machine_name' => 'signboard',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
  );
}
