<?php
/**
 * @file
 * arena_content_type_process.file_type.inc
 */

/**
 * Implements hook_file_default_types().
 */
function arena_content_type_process_file_default_types() {
  $export = array();

  $file_type = new stdClass();
  $file_type->disabled = FALSE; /* Edit this to true to make a default file_type disabled initially */
  $file_type->api_version = 1;
  $file_type->type = 'private_documents';
  $file_type->label = 'Private documents';
  $file_type->description = 'Documents for private space.';
  $file_type->mimetypes = array(
    0 => 'application/msword',
    1 => 'application/vnd.ms-excel',
    2 => 'application/pdf',
    3 => 'application/vnd.ms-powerpoint',
    4 => 'application/vnd.oasis.opendocument.text',
    5 => 'application/vnd.oasis.opendocument.spreadsheet',
    6 => 'application/vnd.oasis.opendocument.presentation',
    7 => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    8 => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    9 => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  );
  $export['private_documents'] = $file_type;

  return $export;
}
