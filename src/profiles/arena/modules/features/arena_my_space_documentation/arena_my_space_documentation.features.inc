<?php
/**
 * @file
 * arena_my_space_documentation.features.inc
 */

/**
 * Implements hook_views_api().
 */
function arena_my_space_documentation_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
