<?php

/*
 * Migrate product
 */
class productMigration extends arenaMigration {

    private $image_source_dir;

    /**
     * Mapping fieldname and tid
     */
    private $signboardTermMap = array(
        'zeus' => 'Hagebaumarkt',
        'bricomarche_france' => 'Bricomarché France',
        'bricomarche_portugal' => 'Bricomarché Portugal',
        'bricomarche_pologne' => 'Bricomarché Pologne',
        'crc_group' => 'Thai Watsadu',
        'jumbo' => 'Jumbo',
        'baucenter' => 'Baucenter',
        'dedeman' => 'Dedeman',
        'brico_io' => 'Brico IO',
        'bricofer' => 'Bricofer',
    );

    /**
     * Implements __contruct() method.
     */
    public function __construct($arguments) {
        parent::__construct($arguments);

        $this->description = t('Migrate product from the Excel source file');

        $this->image_source_dir = DRUPAL_ROOT . base_path() . '../arena-data-for-import/product-images/';

        if (!file_exists($this->image_source_dir)) {
            drupal_set_message(t('Product images directory not found : @filename', array('@filename' => $this->image_source_dir)), 'error');
        }


        $columns = array(
          0 => array('arena_code', t('ARENA Code')),
          1 => array('product_id', t('Product ID')),
          2 => array('picture', t('PICTURE')),
          3 => array('barcode', t('BarCode')),
          4 => array('title', t('Description 1')),
          5 => array('description', t('Description 2 (option)')),
          6 => array('fob_price', t('FOB Price')),

          8 => array('annual_qty_sold', t('Annual quantities sold')),
          9 => array('packaging', t('Packaging')),
          10 => array('unit_pack', t('UNIT PACK')),
          11 => array('moq', t('MOQ (minimum order QTY)')),
          12 => array('quality_marks', t('Quality marks')),
          13  => array('country', t('Country')),
          14 => array('manager_name', t('Market Manager name')),
          15 => array('manager_email', t('Market Manager email')),
          16 => array('new', t('NEW')),
          17 => array('zeus', t('HAGEBAUMARKT')),
          18 => array('bricomarche_france', t('BRICOMARCHE FRANCE')),
          19 => array('bricomarche_portugal', t('BRICOMARCHE PORTUGAL')),
          20 => array('bricomarche_pologne', t('BRICOMARCHE POLOGNE')),
          21 => array('crc_group', t('THAI WATSADU')),
          22 => array('jumbo', t('JUMBO')),
          23 => array('baucenter', t('BAUCENTER')),
          24 => array('dedeman', t('DEDEMAN')),
          25 => array('brico_io', t('BRICO IO')),
          26 => array('bricofer', t('BRICOFER')),
        );

        $csv_file = DRUPAL_ROOT . base_path() . '../arena-data-for-import/products.csv';
        if (!file_exists($csv_file)) {
            drupal_set_message(t('File not found : @filename', array('@filename' => $csv_file)), 'error');
        }

        $this->source = new MigrateSourceCSV(
          $csv_file,
          $columns,
          array(
            'header_rows' => 1,
            'embedded_newlines' => TRUE
          )
        );

        $this->map = new MigrateSQLMap($this->machineName,
          array(
            'product_id' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => TRUE,
              'description' => 'Product ID',
            ),
          ),
          MigrateDestinationNode::getKeySchema()
        );

        $this->destination = new MigrateDestinationNode('product');

        // Language
        $this->addFieldMapping('language')->defaultValue(self::IMPORT_LANGUAGE);

        // Status
        $this->addFieldMapping('status')->defaultValue(NODE_NOT_PUBLISHED);

        // UID
        $this->addFieldMapping('uid')->defaultValue(self::IMPORT_UID);

        // Product name
        $this->addFieldMapping('title', 'title')->description(t('Product name'));
        $this->addFieldMapping('title_field', 'title')->description(t('Product name'));

        // Product ID
        $this->addFieldMapping('field_product_id', 'product_id')->description(t('Product ID'));

        // BarCode
        $this->addFieldMapping('field_product_barcode', 'barcode')->description(t('Barcode'));

        // New
        $this->addFieldMapping('field_product_new', 'new')->description(t('New'))->callbacks(array($this, 'callbackYesNoToBool'));

        // Picture
        $this->addFieldMapping('field_product_image', 'picture')->description(t('Picture'));
        $this->addFieldMapping('field_product_image:preserve_files')->defaultValue(FALSE);
        $this->addFieldMapping('field_product_image:file_replace')->defaultValue(MigrateFile::FILE_EXISTS_REUSE);
        $this->addFieldMapping('field_product_image:destination_dir')->defaultValue('public://product_images/');
        $this->addFieldMapping('field_product_image:source_dir')->defaultValue($this->image_source_dir);

        // Link to nomenclature taxonomy term
        $this->addFieldMapping('field_product_nomenclature', 'arena_code')->description(t('Nomenclature'));
        $this->addFieldMapping('field_product_nomenclature:source_type')->defaultValue('tid');
        $this->addFieldMapping('field_product_nomenclature:create_term')->defaultValue(FALSE);

        // Description
        $this->addFieldMapping('body', 'description')->description(t('Description'))->callbacks('nl2br');
        $this->addFieldMapping('body:format')->defaultValue('filtered_html');

        // FOB price
        $this->addFieldMapping('field_product_fob_price', 'fob_price')->description(t('FOB price'))->callbacks(array($this, 'callbackChangePriceValue'));

        // Annual quantity sold
        $this->addFieldMapping('field_product_annual_sold', 'annual_qty_sold')->description(t('Annual quantity sold'))->callbacks(array($this, 'callbackRemoveSpace'));

        // Packaging
        $this->addFieldMapping('field_product_packaging', 'packaging')->description(t('Packaging'));

        // Unit pack
        $this->addFieldMapping('field_product_unit_pack', 'unit_pack')->description(t('Unit pack'));

        // MOQ
        $this->addFieldMapping('field_product_moq_min_order_qty', 'moq')->description(t('MOQ'))->callbacks(array($this, 'callbackRemoveSpace'));

        // Quality marks
        $this->addFieldMapping('field_product_quality_marks', 'quality_marks')->description(t('Quality marks'))->separator('+');
        $this->addFieldMapping('field_product_quality_marks:create_term')->defaultValue(TRUE);

        // Country
        $this->addFieldMapping('field_product_country', 'country')->description(t('Country'))->callbacks('strtolower');

        // Manager name
        $this->addFieldMapping('field_product_manager_name', 'manager_name')->description(t('Manager name'));

        // Manager Email
        $this->addFieldMapping('field_product_manager_email', 'manager_email')->description(t('Manager Email'));

        // Signboard / Enseigne
        $this->addFieldMapping('field_product_signboard', 'signboard')->description(t('Signboard / Enseigne'));
        $this->addFieldMapping('field_product_signboard:source_type')->defaultValue('name');
        $this->addFieldMapping('field_product_signboard:create_term')->defaultValue(TRUE);

        // Pathauto
        $this->addFieldMapping('pathauto')->defaultValue(TRUE);

        $this->addUnmigratedDestinations(array('created', 'promote', 'sticky', 'translate', 'is_new', 'body:summary',
          'changed', 'revision_uid', 'revision', 'log', 'tnid', 'field_product_image:file_class',
          'field_product_image:destination_file', 'field_product_image:urlencode', 'field_product_image:alt',
          'field_product_image:title', 'field_product_nomenclature:ignore_case', 'field_product_signboard:ignore_case',
          'path', 'comment'));
    }

    /**
     * Implements prepareRow() method.
     * @param $row
     * @return bool
     */
    public function prepareRow($row) {
        // Find nomenclature tid
        $query = db_select('field_data_field_nomenclature_arena_code', 'f');
        $query->addField('f', 'entity_id');
        $query->condition('f.field_nomenclature_arena_code_value', $row->arena_code);
        $query->condition('f.bundle', 'product_nomenclature');
        $query->condition('f.entity_type', 'taxonomy_term');
        $result = $query->execute()->fetchCol();
        $tid = current($result);
        $row->arena_code = $tid;

        // Set signboard
        $row->signboard = array();
        foreach ($this->signboardTermMap as $key => $name) {
            if (isset($row->{$key}) && !empty($row->{$key})) {
                $row->signboard[] = $name;
            }
        }

        // If unit pack is 0 force to 1
        $row->unit_pack = $row->unit_pack == 0 ? 1 : $row->unit_pack;

        // Picture
        if (file_exists($this->image_source_dir.'/'.$row->picture.'.jpg')) {
            $row->picture = $row->picture.'.jpg';
        }
        else if (file_exists($this->image_source_dir.'/'.$row->picture.'.JPG')) {
                $row->picture = $row->picture.'.JPG';
        }
        else {
            $row->picture = '';
        }
        return TRUE;
    }

    /**
     * Callback change 'yes' to 1 and 'no' to 0
     */
    public function callbackYesNoToBool($value) {
        if (strtolower(trim($value)) == 'yes')
            return 1;
        else
            return 0;
    }

    /**
     * Callback remove $ and replace ',' by '.' for price
     */
    public function callbackChangePriceValue($value) {
        $map_replace = array (
          '/\$/' => '',
          '/,/' => '.',
          '/USD/' => '',
        );
        $value = preg_replace(array_keys($map_replace), array_values($map_replace), $value);

        $value = trim($value);
        return $value;
    }

    /**
     * Callback remove space use for number
     */
    public function callbackRemoveSpace($value) {
        $map_replace = array (
          '/[^\d]/' => '',
        );
        $value = preg_replace(array_keys($map_replace), array_values($map_replace), $value);
        return $value;
    }

}
