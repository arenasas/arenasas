<?php

/*
 * Migrate product nomenclature
 */
class nomenclatureMigration extends arenaMigration {

    const TAXONOMY_NAME = 'product_nomenclature';
    const TAXONOMY_VID = 2;
    private $parentsListRollback = array();

    public function __construct($arguments) {
        parent::__construct($arguments);

        $this->description = t('Migrate nomenclature from the Excel source file');

        $columns = array(
          0 => array('arena_code', t('A.R.E.N.A. Code')),
          1 => array('category', t('Category')),
          2 => array('family', t('Family')),
          3 => array('sub_family', t('Sub-family')),
          4 => array('segment', t('Segment')),
          5 => array('product', t('Product')),
        );

        $this->source = new MigrateSourceCSV(
          DRUPAL_ROOT . base_path() . drupal_get_path('module', 'arena_migrate') . '/data/products-nomenclature.csv',
          $columns,
          array(
            'header_rows' => 2,
            'embedded_newlines' => TRUE
          )
        );

        $this->map = new MigrateSQLMap($this->machineName,
          array(
            'arena_code' => array(
              'type' => 'varchar',
              'length' => 128,
              'not null' => TRUE,
              'description' => 'A.R.E.N.A. Code',
            ),
          ),
          MigrateDestinationTerm::getKeySchema()
        );

        $options = array();
        $this->destination = new MigrateDestinationTerm(self::TAXONOMY_NAME, $options);

        // Categorie name
        $this->addFieldMapping('name', 'product')->description(t('Categorie name'));

        // Categorie parent
        $this->addFieldMapping('parent', 'parent')->description(t('Categorie parent'));

        // Categorie ARENA Code
        $this->addFieldMapping('field_nomenclature_arena_code', 'arena_code')->description(t('A.R.E.N.A. code'));
    }

    /**
     * Implements prepare() method.
     */
    public function prepare($term, stdClass $row) {
        // Create Parents
        // Level 1
        $tid = $this->insert_child_term($row, 'category', 0);
        // Level 2
        $tid = $this->insert_child_term($row, 'family', $tid);
        // Level 3
        $tid = $this->insert_child_term($row, 'sub_family', $tid);
        // Level 4
        $tid = $this->insert_child_term($row, 'segment', $tid);

        // Level 5 the last
        $term->weight = $row->csvrownum;
        $term->parent = array($tid);
    }

    /**
     * Create term children, use for level 1 to 4.
     */
    private function insert_child_term($row, $row_field, $parent_tid) {
        // Try to find parent
        $query = db_select('taxonomy_term_data', 't');
        $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = t.tid');
        $query->addField('t', 'tid');
        $query->condition('h.parent', $parent_tid);
        $query->condition('t.name', $row->{$row_field});
        $query->condition('t.vid', self::TAXONOMY_VID);
        $query->addTag('term_access');
        $query->orderBy('t.weight');
        $query->orderBy('t.name');
        $result = $query->execute()->fetchCol();

        // If parent don't exist create parent
        if (empty($result)) {
            $term = new stdClass();
            $term->name = $row->{$row_field};
            $term->parent = $parent_tid;
            $term->weight = $row->csvrownum;
            $term->vid = self::TAXONOMY_VID;
            taxonomy_term_save($term);
            $tid = $term->tid;
        } // Else use parent find with query
        else {
            if (sizeof($result) > 1) {
                drupal_set_message(t('Error'), 'error');
            }
            $tid = current($result);
        }
        return $tid;
    }

    /**
     * Implements prepareRollback() method.
     * Collect parents terms.
     */
    public function prepareRollback($entity_id) {
        // Get parents
        while ($parents = taxonomy_get_parents($entity_id)) {
            if (sizeof($parents) > 1) {
                drupal_set_message(t('Error: Rollback term :tid have :parentsnb parents', array(':tid' => $entity_id, ':parentsnb' => sizeof($parents))), 'error');
            }
            $term = current($parents);
            $entity_id = $term->tid;
            $this->parentsListRollback[] = $entity_id;
        }
    }

    /**
     * Implements completeRollback() method.
     * Delete parents terms.
     */
    public function completeRollback($entity_ids) {
        foreach ($this->parentsListRollback as $tid) {
            $childrens = taxonomy_get_children($tid, self::TAXONOMY_VID);
            if (!sizeof($childrens)) {
                taxonomy_term_delete($tid);
            }
        }
    }
}
