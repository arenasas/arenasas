<?php

function _manage_home()
{
    $renderable = array(
        array(
            '#title' => 'Content',
            '#type' => 'fieldset',
            array(
                '#markup' => '<a href="/node/add/ally">Add Ally</a><br/>',
            ),
            array(
                '#markup' => '<a href="/manage/content">Manage content</a><br/>',
            ),
        ),
        array(
            '#type' => 'fieldset',
            '#title' => 'Users',
            array(
                '#markup' => '<a href="/manage/users">Manage users</a><br/>',
            ),
        ),
        array(
            '#title' => 'Contact',
            '#type' => 'fieldset',
            array(
                '#markup' => '<a href="/admin/config/arena/contact">Manage contact form</a><br/>',
            ),
        ),
        array(
            '#type' => 'fieldset',
            '#title' => 'Private space',
            array(
                '#markup' => '<a href="/node/add/product">Add Product</a><br/>',
            ),
            array(
                '#markup' => '<a href="/node/add/newsletter">Add Newsletter</a><br/>',
            ),
            array(
                '#markup' => '<a href="/admin/structure/taxonomy/signboard">Manage banners</a><br/>',
            ),
            array(
                '#markup' => '<a href="/manage/private-content">Manage private content</a><br/>',
            ),
            array(
                '#markup' => '<a href="/admin/config/arena/my-space">Manage home page</a><br/>',
            ),
            array(
                '#type' => 'fieldset',
                '#title' => 'Documentation',
                array(
                    '#markup' => '<a href="/file/add">Add document</a><br/>',
                ),
                array(
                    '#markup' => '<a href="admin/structure/taxonomy/document_category">Manage document categories</a><br/>',
                ),
                array(
                    '#markup' => '<a href="/manage/documentation">Manage documentation</a><br/>',
                ),
            ),
            array(
                '#type' => 'fieldset',
                '#title' => 'Supplier profiles',
                array(
                    '#markup' => '<a href="/manage/supplier-profiles">Manage supplier profiles</a><br/>',
                ),
                array(
                    '#markup' => '<a href="/admin/config/arena/supplier/emails">Manage notification emails</a><br/>',
                ),
            ),
        ),
    );
    return $renderable;
}
