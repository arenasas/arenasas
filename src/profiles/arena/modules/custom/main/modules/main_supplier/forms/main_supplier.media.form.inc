<?php

function _main_supplier_media_form($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    if (isset($profile->field_media[LANGUAGE_NONE][0]['value'])) {
        $values = json_decode($profile->field_media[LANGUAGE_NONE][0]['value'], TRUE);
    }

    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );

    $form['fieldset_1']['picture'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#attributes' => array(),
    );

    $i = 0;
    if (isset($values['picture'])) {
        foreach ($values['picture'] as $picture) {
            $file = file_load($picture['file']);
            $form['fieldset_1']['picture'][] = array(
                '#type' => 'container',
                '#attributes' => array('class' => array('file-item-container')),
                'title'  => array(
                    '#type' => 'hidden',
                    '#value' => $picture['title'],
                ),
                'file'  => array(
                    '#type' => 'hidden',
                    '#value' => $picture['file'],
                ),
                'picture_display_' . ($i + 1) => array(
                    '#markup' => l($file->filename, file_create_url($file->uri), array('attributes' => array('target' => '_blank'))) . '<a class="button">Remove</a>'
                ),
            );
        }
    }
    $form['fieldset_1']['new_picture'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'new_title' => array(
            '#type' => 'textfield',
            '#title' => 'File title',
        ),
        'new_file' => array(
            '#type' => 'file',
            '#name' => 'files[new_file]',
            '#description' => t('Upload file, allowed extensions: jpg, jpeg, png, gif'),
        )
    );

    return _main_supplier_formalize($form);
}

/**
 * Validate callback.
 */
function _main_supplier_media_form_validate($form, &$form_state)
{
    $file = file_save_upload('new_file', array(
        'file_validate_extensions' => array('jpg jpeg png gif'),
    ));
    if ($file) {
        if ($file = file_move($file, 'private://')) {
            $file->status = FILE_STATUS_PERMANENT;
            file_save($file);

            if (isset($form_state['values']['new_picture']['new_file'])) {
                $form_state['values']['picture'][] = array(
                    'title' => $form_state['values']['new_picture']['new_title'],
                    'file' => $file->fid,
                );
            }
        } else {
            form_set_error('files[new_picture', t('Failed to write the uploaded file to the site\'s file folder.'));
        }
    }
    unset($form_state['values']['new_picture']);
}

/**
 * Submit callback.
 */
function _main_supplier_media_form_submit($form, &$form_state)
{
    $values = $form_state['values'];

    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op') as $value) {
        unset($values[$value]);
    }

    if (isset($form_state['input']['picture'])) {
        foreach ($form_state['input']['picture'] as $key => $picture) {
            if (empty($picture['file']) || $picture['file'] == 0) {
                array_splice($values['picture'], $key, 1);
            }
        }
    }

    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    $w_profile = entity_metadata_wrapper('node', $profile);
    $w_profile->field_media = json_encode($values);
    $w_profile->save();

    drupal_set_message('Information has been saved.');
}
