<?php

function _main_supplier_general_information_form($form)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $is_validated = MainSupplierAPI::isProfileValidated($nid);
    $profile = node_load($nid);
    if (isset($profile->field_general_information[LANGUAGE_NONE][0]['value'])) {
        $values = json_decode($profile->field_general_information[LANGUAGE_NONE][0]['value'], TRUE);
    }

    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#title' => 'Type of business',
        '#attributes' => array('class' => array('supplier-form-container')),
    );
    $form['fieldset_1']['type_of_business'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Type of business',
        '#options' => array(
            'manufacturing' => 'Manufacturing',
            'trading_import_export' => 'Trading / Imp-exp company',
            'agent' => 'Agent',
            'other' => 'Other',
        ),
        '#default_value' => isset($values['type_of_business']) ? $values['type_of_business'] : array(),
        '#required' => TRUE,
    );
    $form['fieldset_1']['type_of_business_precise'] = array(
        '#type' => 'textfield',
        '#title' => 'Please precise',
        '#default_value' => isset($values['type_of_business_precise']) ? $values['type_of_business_precise'] : '',
    );

    if ($is_validated) {
        $form['fieldset_2'] = array(
            '#type' => 'fieldset',
            '#title' => 'Ownership',
            '#attributes' => array('class' => array('supplier-form-container')),
        );
        $form['fieldset_2']['ownership'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Ownership',
            '#options' => array(
                'corporation' => 'Corporation',
                'sole_proprieto' => 'Sole proprieto',
                'state_owned' => 'State owned',
                'joint_venture' => 'Joint venture',
                'partnership' => 'Partnership',
                'other' => 'Other',
            ),
            '#default_value' => isset($values['ownership']) ? $values['ownership'] : array(),
            '#required' => TRUE,
        );
        $form['fieldset_2']['ownership_precise'] = array(
            '#type' => 'textfield',
            '#title' => 'Please precise',
            '#default_value' => isset($values['ownership_precise']) ? $values['ownership_precise'] : '',
        );
        $form['fieldset_2']['nationality_of_ownership'] = array(
            '#type' => 'select',
            '#title' => 'Nationality of ownership',
            '#description' => 'The countries on the top of the list correspond to our allies\' markets.',
            '#options' => country_get_list(),
            '#empty_value' => '',
            '#default_value' => isset($values['nationality_of_ownership']) ? $values['nationality_of_ownership'] : array(),
            '#required' => TRUE,
        );
    }

    $form['fieldset_3'] = array(
        '#type' => 'fieldset',
        '#title' => 'Product subfamilies',
        '#attributes' => array('class' => array('supplier-form-container')),
        '#description' => 'Your major product families (max. 5)',
        'product_subfamilies' => array(
            '#type' => 'fieldset',
            '#tree' => TRUE,
        ),
    );

    $product_nomenclature = taxonomy_vocabulary_machine_name_load('product_nomenclature');
    $products_categories = taxonomy_get_tree($product_nomenclature->vid, 0, 1);
    foreach ($products_categories as $products_category) {
        $options = array();
        $tree = taxonomy_get_tree($product_nomenclature->vid, $products_category->tid, 1);
        foreach($tree as $branch) {
            $subfamily = taxonomy_get_tree($product_nomenclature->vid, $branch->tid, 1);
            foreach($subfamily as $subfamily_branch) {
                $options[$subfamily_branch->tid] = $subfamily_branch->name;
            }
        }

        $form['fieldset_3']['product_subfamilies'][$products_category->tid] = array(
            '#title' => $products_category->name,
            '#type' => 'checkboxes',
            '#options' => $options,
            '#default_value' => isset($values['product_subfamilies'][$products_category->tid]) ? $values['product_subfamilies'][$products_category->tid] : array(),
        );
    }

    return _main_supplier_formalize($form);
}

//validate
function _main_supplier_general_information_form_validate($form, &$form_state) {

    $form_state['values']['type_of_business'] = array_filter($form_state['values']['type_of_business']);

    if (!empty($form_state['values']['type_of_business']['other']) && empty($form_state['values']['type_of_business_precise'])) {
        form_set_error('type_of_business_precise', 'If you check other, you must precise.');
    }
    if (!empty($form_state['values']['ownership']['other']) && empty($form_state['values']['ownership_precise'])) {
        form_set_error('type_of_business_precise', 'If you check other, you must precise.');
    }

    $count = 0;
    foreach ($form_state['values']['product_subfamilies'] as $key => $value) {
        $form_state['values']['product_subfamilies'][$key] = array_filter($form_state['values']['product_subfamilies'][$key]);
        $count += count($form_state['values']['product_subfamilies'][$key]);
    }
    if (0 == $count || $count > 5) {
        form_set_error('fieldset_3][product_subfamilies', 'At least one, and maximum five product subfamilies are required');
    }
}

//submit
function _main_supplier_general_information_form_submit($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    $w_profile = entity_metadata_wrapper('node', $profile);
    $values = $form_state['values'];

    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op') as $value) {
        unset($values[$value]);
    }


    $w_profile->field_general_information = json_encode($values);
    $w_profile->save();

    drupal_set_message('Information has been saved.');

    if (MainSupplierAPI::isProfileValidated($nid)) {
        drupal_goto('supplier/additional-information');
    }
}
