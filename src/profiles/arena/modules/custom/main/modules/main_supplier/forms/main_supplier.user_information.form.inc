<?php


function _main_supplier_user_information_form()
{
    $uid = MainSupplierAPI::getCurrentUid();
    $account = user_load($uid);

    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['fieldset_1']['email'] = array(
        '#type' => 'textfield',
        '#title' => 'Email',
        '#default_value' => isset($account->mail) ? $account->mail : '',
        '#required' => TRUE,
    );
    $form['fieldset_1']['first_name'] = array(
        '#type' => 'textfield',
        '#title' => 'First name',
        '#default_value' => isset($account->field_first_name[LANGUAGE_NONE][0]['value']) ? $account->field_first_name[LANGUAGE_NONE][0]['value'] : '',
        '#required' => TRUE,
    );
    $form['fieldset_1']['last_name'] = array(
        '#type' => 'textfield',
        '#title' => 'Last name',
        '#default_value' => isset($account->field_last_name[LANGUAGE_NONE][0]['value']) ? $account->field_last_name[LANGUAGE_NONE][0]['value'] : '',
        '#required' => TRUE,
    );

    return _main_supplier_formalize($form);
}

//validate
function _main_supplier_user_information_form_validate($form, &$form_state)
{
    $uid = MainSupplierAPI::getCurrentUid();
    $values = $form_state['values'];

    if (!valid_email_address($values['email'])) {
        form_set_error('email', t('Invalid email.'));
    } else {
        if ($uid == 0) {
            if (user_load_by_name($values['email']) !== FALSE || user_load_by_mail($values['email']) !== FALSE) {
                form_set_error('email', 'Email already exists. If you already have an account, please login.');
            }
        }
    }
}

//submit
function _main_supplier_user_information_form_submit($form, &$form_state)
{
    $values = $form_state['values'];

    $uid = MainSupplierAPI::getCurrentUid();
    $edit = array();
    if ($uid === 0) {
        $account = MainSupplierAPI::createAccount($values['email'], $values['first_name'], $values['last_name']);
    } else {
        $account = user_load($uid);
        $edit['name'] = $values['email'];
        $edit['mail'] = $values['email'];
        $edit['field_first_name'][LANGUAGE_NONE][0]['value'] = $values['first_name'];
        $edit['field_last_name'][LANGUAGE_NONE][0]['value'] = $values['last_name'];
        $account = user_save($account, $edit);
    }

    MainSupplierAPI::setCurrentUid($account->uid);

    drupal_set_message('Information has been saved.');

    drupal_goto('supplier/description');
}
