<?php

function _main_supplier_market_client_information_form($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);

    if (isset($profile->field_market_client_information[LANGUAGE_NONE][0]['value'])) {
        $values = json_decode($profile->field_market_client_information[LANGUAGE_NONE][0]['value'], TRUE);
    }

    $types = array('country', 'product_category');
    foreach ($types as $type) {
        if (isset($form_state['triggering_element']['#ajax']['callback'])) {
            if ($form_state['triggering_element']['#ajax']['callback'] == '_main_supplier_additional_information_form_ajax_add_' . $type) {
                $form_state['storage'][$type . '_count']++;
            } elseif ($form_state['triggering_element']['#ajax']['callback'] == '_main_supplier_additional_information_form_ajax_remove_' . $type) {
                $form_state['storage'][$type . '_count']--;
            }
        } else {
            switch ($type) {
                case 'product_category':
                    if (isset($values['product_category'])) {
                        $form_state['storage'][$type . '_count'] = count($values['product_category']);
                    }
                    break;
                case 'country':
                    if (isset($values['country'])) {
                        $form_state['storage'][$type . '_count'] = count($values['country']);
                    }
                    break;
            }

            if (!isset($form_state['storage'][$type . '_count'])) {
                $form_state['storage'][$type . '_count'] = 1;
            }
        }
    }

    if (isset($form_state['triggering_element']['#ajax']['callback'])) {
        $index = _main_supplier_get_country_index($form_state['triggering_element']['#id']);
        if ($form_state['triggering_element']['#ajax']['callback'] == '_main_supplier_additional_information_form_ajax_add_turnover_per_ally') {
            $form_state['storage']['turnover_per_ally_' . $index . '_count']++;
        } elseif ($form_state['triggering_element']['#ajax']['callback'] == '_main_supplier_additional_information_form_ajax_remove_turnover_per_ally') {
            $form_state['storage']['turnover_per_ally_' . $index . '_count']--;
        }
    }
    for ($i = 1; $i <= $form_state['storage']['country_count']; $i++) {
        if (!isset($form_state['storage']['turnover_per_ally_' . $i . '_count'])) {

            if (isset($values['country'][$i]['turnover_per_ally'])) {
                $form_state['storage']['turnover_per_ally_' . $i . '_count'] = count($values['country'][$i]['turnover_per_ally']);
            } else {
                $form_state['storage']['turnover_per_ally_' . $i . '_count'] = 1;
            }
        }
    }

    $product_subfamilies = MainSupplierAPI::getProductSubFamilies($nid);

    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#title' => 'Activity per country',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );

    $allies = MainSupplierAPI::getAllies();
    $turnover_per_ally_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'allies' => array(
            '#type' => 'select',
            '#title' => 'Allies',
            '#empty_value' => '',
            '#required' => TRUE,
            '#options' => $allies,
        ),
        'turnover' => array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => 'Turnover',
            '#element_validate' => array('element_validate_integer_positive'),
        ),
    );

    $country_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'country' => array(
            '#type' => 'select',
            '#title' => 'Country',
            '#description' => 'The countries on the top of the list correspond to our allies\' markets.',
            '#empty_value' => '',
            '#required' => TRUE,
            '#options' => country_get_list(),
        ),
        'turnover_percentage' => array(
            '#type' => 'textfield',
            '#title' => 'Turnover percentage',
        ),
        'turnover' => array(
            '#type' => 'textfield',
            '#title' => 'Turnover in value',
        ),
        'market_share' => array(
            '#type' => 'textfield',
            '#title' => 'Market share percentage',
        ),
        'main_sector_of_activity' => array(
            '#type' => 'textfield',
            '#title' => 'Main sector of activity',
            '#description' => 'DIY, Wholesaler, Professional, Food Chain…',
        ),
        'main_customers_name' => array(
            '#type' => 'textfield',
            '#title' => 'Main customers name',
        ),
        'product_subfamilies' => array(
            '#type' => 'checkboxes',
            '#title' => 'Product subfamilies',
            '#options' => $product_subfamilies,
        ),
        'turnover_per_ally' => array(
            '#type' => 'container',
            '#tree' => TRUE,
            '#suffix' => '</div>',
            '#attributes' => array(),
        ),
        'add_turnover_per_ally' => array(
            '#type' => 'button',
            '#href' => '',
            '#limit_validation_errors' => array(),
            '#ajax' => array(
                'callback' => '_main_supplier_additional_information_form_ajax_add_turnover_per_ally',
            ),
        ),
        'remove_turnover_per_ally' => array(
            '#type' => 'button',
            '#href' => '',
            '#limit_validation_errors' => array(),
            '#ajax' => array(
                'callback' => '_main_supplier_additional_information_form_ajax_remove_turnover_per_ally',
            ),
        ),
    );

    $form['fieldset_1']['country'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="country">',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_1']['add_country'] = array(
        '#type' => 'button',
        '#value' => 'Add country',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_add_country',
            'wrapper' => 'country',
        ),
    );
    $form['fieldset_1']['remove_country'] = array(
        '#type' => 'button',
        '#value' => 'Remove country',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_remove_country',
            'wrapper' => 'country',
        ),
    );

    for ($i = 1; $i <= $form_state['storage']['country_count']; $i++) {
        $country_form['country']['#default_value'] = isset($values['country'][$i]['country']) ? $values['country'][$i]['country'] : '';
        $country_form['turnover_percentage']['#default_value'] = isset($values['country'][$i]['turnover_percentage']) ? $values['country'][$i]['turnover_percentage'] : '';
        $country_form['turnover']['#default_value'] = isset($values['country'][$i]['turnover']) ? $values['country'][$i]['turnover'] : '';
        $country_form['market_share']['#default_value'] = isset($values['country'][$i]['market_share']) ? $values['country'][$i]['market_share'] : '';
        $country_form['main_sector_of_activity']['#default_value'] = isset($values['country'][$i]['main_sector_of_activity']) ? $values['country'][$i]['main_sector_of_activity'] : '';
        $country_form['main_customers_name']['#default_value'] = isset($values['country'][$i]['main_customers_name']) ? $values['country'][$i]['main_customers_name'] : '';
        $country_form['product_subfamilies']['#default_value'] = isset($values['country'][$i]['product_subfamilies']) ? $values['country'][$i]['product_subfamilies'] : array();

        $country_form['turnover_per_ally']['#prefix'] = '<div id="turnover-per-ally-' . $i . '"><span class="panel-subtitle">Turnover in value with A.R.E.N.A members (per banner)</span>';
        $country_form['add_turnover_per_ally']['#ajax']['wrapper'] = 'turnover-per-ally-' . $i;
        $country_form['add_turnover_per_ally']['#value'] = 'Add turnover ' . $i . ' per ally';
        $country_form['remove_turnover_per_ally']['#ajax']['wrapper'] = 'turnover-per-ally-' . $i;
        $country_form['remove_turnover_per_ally']['#value'] = 'Remove turnover ' . $i . ' per ally';

        $form['fieldset_1']['country'][$i] = $country_form;

        for ($j = 1; $j <= $form_state['storage']['turnover_per_ally_' . $i . '_count']; $j++) {
            $form['fieldset_1']['country'][$i]['turnover_per_ally'][$j] = $turnover_per_ally_form;
            if (isset($values['country'][$i]['turnover_per_ally'][$j])) {
                $form['fieldset_1']['country'][$i]['turnover_per_ally'][$j]['allies']['#default_value'] = $values['country'][$i]['turnover_per_ally'][$j]['allies'];
                $form['fieldset_1']['country'][$i]['turnover_per_ally'][$j]['turnover']['#default_value'] = $values['country'][$i]['turnover_per_ally'][$j]['turnover'];
            } else {
                $form['fieldset_1']['country'][$i]['turnover_per_ally'][$j]['allies']['#default_value'] = '';
                $form['fieldset_1']['country'][$i]['turnover_per_ally'][$j]['turnover']['#default_value'] = '';
            }
        }
    }

    $form['fieldset_2'] = array(
        '#type' => 'fieldset',
        '#title' => 'Activity per product category',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $product_category_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'product_options' => array(
            '#type' => 'select',
            '#title' => 'Product subfamilies',
            '#empty_value' => '',
            '#required' => TRUE,
            '#options' => $product_subfamilies,
        ),
        'turnover_percentage' => array(
            '#type' => 'textfield',
            '#title' => 'Turnover percentage',
            '#description' => 'In percentage'
        ),
        'turnover' => array(
            '#type' => 'textfield',
            '#title' => 'Turnover in value',
        ),
        'main_brands' => array(
            '#type' => 'textfield',
            '#title' => 'Main brands',
        ),
    );
    $form['fieldset_2']['product_category'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="product-category">',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_2']['add_product_category'] = array(
        '#type' => 'button',
        '#value' => 'Add product category',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_add_product_category',
            'wrapper' => 'product-category',
        ),
    );
    $form['fieldset_2']['remove_product_category'] = array(
        '#type' => 'button',
        '#value' => 'Remove product_category',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_remove_product_category',
            'wrapper' => 'product-category',
        ),
    );
    for ($i = 1; $i <= $form_state['storage']['product_category_count']; $i++) {
        if (isset($values['product_category'][$i])) {
            $product_category_form['product_options']['#default_value'] = $values['product_category'][$i]['product_options'];
            $product_category_form['turnover_percentage']['#default_value'] = $values['product_category'][$i]['turnover_percentage'];
            $product_category_form['turnover']['#default_value'] = $values['product_category'][$i]['turnover'];
            $product_category_form['main_brands']['#default_value'] = $values['product_category'][$i]['main_brands'];
        } else {
            $product_category_form['product_options']['#default_value'] = '';
            $product_category_form['turnover_percentage']['#default_value'] = '';
            $product_category_form['turnover']['#default_value'] = '';
            $product_category_form['main_brands']['#default_value'] = '';
        }
        $form['fieldset_2']['product_category'][$i] = $product_category_form;
    }

    return _main_supplier_formalize($form);
}

/**
 * Validate form callback.
 */
function _main_supplier_market_client_information_form_validate($form, &$form_state) {
    $values = $form_state['values'];

    // Activity per country
    $fields_infos = array(
        array('name' => 'turnover_percentage', 'type' => 'number'),
        array('name' => 'market_share', 'type' => 'number'),
        array('name' => 'turnover', 'type' => 'integer'),
    );
    
    
    foreach ($values['country'] as $i => $country) {
      
        foreach ($fields_infos as $field_infos) {
            if (isset($values['country'][$i][$field_infos['name']]) && !empty($values['country'][$i][$field_infos['name']])) {
                $value = $values['country'][$i][$field_infos['name']];
                if (!_main_supplier_validate_numeric_positive($value, $field_infos['type'])) {
                    form_set_error('country' . '][' . $i . '][' . $field_infos['name'], t('Invalid "@fieldname" value for "@groupname", you need to give a positive @type.',
                      array(
                        '@fieldname' => $form['fieldset_1']['country'][$i][$field_infos['name']]['#title'],
                        '@groupname' => $form['fieldset_1']['#title'],
                        '@type' => $field_infos['type']
                      )
                    ));
                }
            }
        }
    }

    // Activity per product category
    $fields_infos = array(
        array('name' => 'turnover_percentage','type' => 'number'), 
        array('name' => 'turnover', 'type' => 'integer'),
    );
    foreach ($values['product_category'] as $i => $country) {
        foreach ($fields_infos as $field_infos) {
            if (isset($values['product_category'][$i][$field_infos['name']]) && !empty($values['product_category'][$i][$field_infos['name']])) {
                $value = $values['product_category'][$i][$field_infos['name']];
                if (!_main_supplier_validate_numeric_positive($value, $field_infos['type'])) {
                    form_set_error('product_category' . '][' . $i . '][' . $field_infos['name'], t('Invalid "@fieldname" value for "@groupname", you need to give a positive @type.',
                      array(
                        '@fieldname' => $form['fieldset_2']['product_category'][$i][$field_infos['name']]['#title'],
                        '@groupname' => $form['fieldset_2']['#title'],
                        '@type' => $field_infos['type']
                      )
                    ));
                }
            }
        }
    }
}

//submit
function _main_supplier_market_client_information_form_submit($form, &$form_state) {
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    $w_profile = entity_metadata_wrapper('node', $profile);
    $values = $form_state['values'];

    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op', 'add_country', 'remove_country', 'add_product_category', 'remove_product_category') as $value) {
        unset($values[$value]);
    }

    $w_profile->field_market_client_information = json_encode($values);
    $w_profile->save();

    drupal_set_message('Information has been saved.');

    drupal_goto('supplier/production-information');
}

/**********
 ** AJAX **
 **********/
function _main_supplier_additional_information_form_ajax_add_country($form, &$form_state)
{
    return $form['fieldset_1']['country'];
}

function _main_supplier_additional_information_form_ajax_remove_country($form, &$form_state)
{
    return $form['fieldset_1']['country'];
}

function _main_supplier_additional_information_form_ajax_add_turnover_per_ally($form, &$form_state)
{
    $index = _main_supplier_get_country_index($form_state['triggering_element']['#id']);
    return $form['fieldset_1']['country'][$index]['turnover_per_ally'];
}

function _main_supplier_additional_information_form_ajax_remove_turnover_per_ally($form, &$form_state)
{
    $index = _main_supplier_get_country_index($form_state['triggering_element']['#id']);
    return $form['fieldset_1']['country'][$index]['turnover_per_ally'];
}

function _main_supplier_additional_information_form_ajax_add_product_category($form, &$form_state)
{
    return $form['fieldset_2']['product_category'];
}

function _main_supplier_additional_information_form_ajax_remove_product_category($form, &$form_state)
{
    return $form['fieldset_2']['product_category'];
}

//util
function _main_supplier_get_country_index($id) {
    $id = str_replace('edit-country-', '', $id);
    $index = $id[0];
    return $index;
}
