<?php

class MainSupplierAPI {

    const MODERATION_STATUS_NEW = 0;
    const MODERATION_STATUS_AWAITING_PROFILE_VALIDATION = 1;
    const MODERATION_STATUS_PROFILE_VALIDATED = 2;

    public static function isDeviceGranted() {
        if (preg_match('#iPad|iPhone|Android#', $_SERVER['HTTP_USER_AGENT'])) {
            return FALSE;
        }
        return TRUE;
    }

    public static function getCurrentUid() {
        global $user;

        if ($user->uid > 0) {
            return $user->uid;
        } elseif (isset($_COOKIE['main_supplier_uid'])) {
            return $_COOKIE['main_supplier_uid'];
        } else {
            return 0;
        }
    }

    /**
     *  Get supplier page path with language prefix
     */
    public static function getSupplierPath() {
        return url('supplier').'/';
    }

    public static function setCurrentUid($uid) {
        global $user;

        if ($user->uid === 0) {
            $supplier_path = self::getSupplierPath();
            setcookie('main_supplier_uid', $uid, 0, $supplier_path, NULL, FALSE, TRUE);
        }
    }

    public static function getCurrentNid() {
        global $user;
        if (isset($_COOKIE['main_supplier_nid'])) {
            return $_COOKIE['main_supplier_nid'];
        } else {
            $profile_nid = self::getUserProfileNid($user->uid);
            return $profile_nid;
        }
    }

    public static function setCurrentNid($nid) {
        global $user;

        if ($user->uid === 0) {
            $supplier_path = self::getSupplierPath();
            setcookie('main_supplier_nid', $nid, 0, $supplier_path, NULL, FALSE, TRUE);
        }
    }

    public static function resetCurrentData() {
        $supplier_path = self::getSupplierPath();
        setcookie('main_supplier_uid', NULL, -1, $supplier_path);
        setcookie('main_supplier_nid', NULL, -1, $supplier_path);
    }

    public static function createAccount($email, $first_name, $last_name) {

        $account = new stdClass();
        $account->name = $email;
        $account->pass = user_password();
        $account->mail = $email;
        $account->status = 0;

        $supplier_role = user_role_load_by_name('suppliers');
        $roles = array($supplier_role->rid => 'suppliers');
        $account->roles = $roles;

        $account->field_first_name[LANGUAGE_NONE][0]['value'] = $first_name;
        $account->field_last_name[LANGUAGE_NONE][0]['value'] = $last_name;

        return user_save($account);
    }

    //the arguments should be sanitized before being passed to this function
    public static function createProfile($uid, $name) {
        $node = new stdClass();
        
        $node->title = $name;
        $node->type = 'supplier_profile';
        $node->language = LANGUAGE_NONE;
        $node->uid = $uid;
        $node->status = 0;

        node_save($node);
        return $node;
    }

    //send emails at various points of the profile workflow
    public static function notify($to, $key, $params = array()) {
        if ($to == 'admins') {
            $uids = array();
            $query = 'SELECT users.name, users.mail
                      FROM role
                      LEFT JOIN users_roles ON users_roles.rid = role.rid
                      LEFT JOIN users ON users_roles.uid = users.uid
                      WHERE role.name = :role';
            $result = db_query($query, array(':role' => 'admin'));

            $to = array();
            foreach ($result as $row) {
                $to[] = $row->mail;
            }
            $to = implode(', ', $to);
        }
        drupal_mail('main_supplier', $key, $to, 'en', $params);
    }

    public static function isStepValid($step_index) {
        switch ($step_index) {
            case 1:
                $uid = MainSupplierAPI::getCurrentUid();
                return $uid > 0;
                break;
            case 2:
                $nid = MainSupplierAPI::getCurrentNid();
                return $nid > 0;
                break;
            case 3:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_general_information[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            case 4:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_additional_information[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            case 5:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_market_client_information[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            case 6:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_production_information[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            case 7:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_quality_control[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            case 8:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_communication_documents[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            case 9:
                $nid = MainSupplierAPI::getCurrentNid();
                if ($nid > 0) {
                    $profile = node_load($nid);
                    return isset($profile->field_media[LANGUAGE_NONE][0]['value']);
                } else {
                    return FALSE;
                }
                break;
            default:
                return FALSE;
                break;
        }
    }

    public static function getUserProfileNid($uid) {
        
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'supplier_profile')
            ->propertyCondition('uid', $uid)
            ->addMetaData('account', user_load(1)); // Run the query as user 1.
        $result = $query->execute();

        if (isset($result['node'])) {
            $keys = array_keys($result['node']);
            return $keys[0];
        }
        return 0;
    }

    public static function getProductSubFamiliesForNodeLoad(&$node) {
        $wnode = entity_metadata_wrapper('node', $node);
        $general_information = json_decode($wnode->field_general_information->value(), TRUE);

        $product_subfamilies_tid = array();
        if (isset($general_information['product_subfamilies'])) {
            $product_subfamilies = $general_information['product_subfamilies'];
            foreach ($product_subfamilies as $tids) {
                if (!is_array($tids)) {
                    $tids = array($tids);
                }
                foreach ($tids as $tid) {
                    $product_subfamilies_tid[]['tid'] = $tid;
                }
            }
        }
        return ($product_subfamilies_tid);
    }

    public static function getProductSubFamilies($nid) {
        $profile = node_load($nid);

        if (empty($profile->field_general_information)) {
            return array();
        }

        $general_information = $profile->field_general_information[LANGUAGE_NONE][0]['value'];
        $general_information = json_decode($general_information, TRUE);

        $subfamilies = array();
        if (!isset($general_information['product_subfamilies'])) {
            return $subfamilies;
        }
        foreach($general_information['product_subfamilies'] as $product_category) {
            if (!is_array($product_category)) {
                $product_category = array($product_category => $product_category);
            }
            foreach ($product_category as $key => $value) {
                $value = taxonomy_term_load($value);
                $subfamilies[$value->tid] = $value->name;
            }
        }
        return $subfamilies;
    }

    public static function getAllies() {
        $signboard = taxonomy_vocabulary_machine_name_load('signboard');
        $tree = taxonomy_get_tree($signboard->vid);
        $allies = array();
        foreach ($tree as $branch) {
            $allies[$branch->tid] = $branch->name;
        }
        return $allies;
    }

    public static function isProfileValidated($nid) {
        $profile = node_load($nid);
        if ($nid > 0 && isset($profile)) {
            $moderation_status = isset($profile->field_moderation_status[LANGUAGE_NONE][0]['value']) ? $profile->field_moderation_status[LANGUAGE_NONE][0]['value'] : NULL;
            if ($moderation_status !== NULL && $moderation_status == MainSupplierAPI::MODERATION_STATUS_PROFILE_VALIDATED) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function getCurrency($node, $symbol = FALSE) {
        if (is_numeric($node)) {
            $node = node_load($node);
        }
        if (isset($node->field_additional_information[LANGUAGE_NONE][0]['value'])) {
            $value = json_decode($node->field_additional_information[LANGUAGE_NONE][0]['value'], TRUE);
            if (isset($value['currency'])) {
                if ($symbol) {
                    if ($value['currency'] == 'USD') {
                        return '$';
                    } elseif ($value['currency'] == 'EUR') {
                        return '€';
                    }
                } else {
                    return $value['currency'];
                }
            }
            return NULL;
        }
        return NULL;
    }
}
