<?php

require 'forms/main_supplier.form.inc';
require 'forms/main_supplier.user_information.form.inc';
require 'forms/main_supplier.description.form.inc';
require 'forms/main_supplier.general_information.form.inc';
require 'forms/main_supplier.additional_information.form.inc';
require 'forms/main_supplier.market_client_information.form.inc';
require 'forms/main_supplier.production_information.form.inc';
require 'forms/main_supplier.quality_control.form.inc';
require 'forms/main_supplier.media.form.inc';
require 'forms/main_supplier.communication_documents.form.inc';


/**
 * Implements hook_menu().
 */
function main_supplier_menu()
{
    return array(
        'supplier' => array(
            'title' => 'Supplier',
            'page callback' => '_main_supplier',
            'file' => 'main_supplier.pages.inc',
            'access callback' => TRUE,
            'type' => MENU_CALLBACK,
        ),
        'my-space/suppliers-directory/%node' => array(
            'page callback' => '_main_supplier_alias',
            'page arguments' => array(2),
            'file' => 'main_supplier.pages.inc',
            'access arguments' => array('access my space'),
            'type' => MENU_CALLBACK,
        ),
        'supplier/user-information' => array(
            'title' => 'User information',
            'page callback' => '_main_supplier_user_information',
            'file' => 'main_supplier.pages.inc',
            'access callback' => TRUE,
            'type' => MENU_CALLBACK,
        ),
        'supplier/description' => array(
            'title' => 'Description',
            'page callback' => '_main_supplier_description',
            'file' => 'main_supplier.pages.inc',
            'access callback' => TRUE,
            'type' => MENU_CALLBACK,
        ),
        'supplier/general-information' => array(
            'title' => 'General information',
            'page callback' => '_main_supplier_general_information',
            'file' => 'main_supplier.pages.inc',
            'access callback' => TRUE,
            'type' => MENU_CALLBACK,
        ),
        'supplier/additional-information' => array(
            'title' => 'Additional information',
            'page callback' => '_main_supplier_additional_information',
            'file' => 'main_supplier.pages.inc',
            'access callback' => '_main_supplier_has_enabled_profile',
            'type' => MENU_CALLBACK,
        ),
        'supplier/market-client-information' => array(
            'title' => 'Market / Client information',
            'page callback' => '_main_supplier_market_client_information',
            'file' => 'main_supplier.pages.inc',
            'access callback' => '_main_supplier_has_enabled_profile',
            'type' => MENU_CALLBACK,
        ),
        'supplier/production-information' => array(
            'title' => 'Production information',
            'page callback' => '_main_supplier_production_information',
            'file' => 'main_supplier.pages.inc',
            'access callback' => '_main_supplier_has_enabled_profile',
            'type' => MENU_CALLBACK,
        ),
        'supplier/quality-control' => array(
            'title' => 'Quality control',
            'page callback' => '_main_supplier_quality_control',
            'file' => 'main_supplier.pages.inc',
            'access callback' => '_main_supplier_has_enabled_profile',
            'type' => MENU_CALLBACK,
        ),
//        'supplier/media' => array(
//            'title' => 'Media',
//            'page callback' => '_main_supplier_media',
//            'file' => 'main_supplier.pages.inc',
//            'access callback' => '_main_supplier_has_enabled_profile',
//            'type' => MENU_CALLBACK,
//        ),
//        'supplier/communication-documents' => array(
//            'title' => 'Communication Documents',
//            'page callback' => '_main_supplier_communication_documents',
//            'file' => 'main_supplier.pages.inc',
//            'access callback' => '_main_supplier_has_enabled_profile',
//            'type' => MENU_CALLBACK,
//        ),
        'admin/config/arena/supplier/emails' => array(
            'type' => MENU_CALLBACK,
            'file' => 'main_supplier.admin.inc',
            'page callback' => 'drupal_get_form',
            'page arguments' => array('_main_supplier_emails_admin_form'),
            'access callback' => 'user_access',
            'access arguments' => array('manage supplier notification emails'),
        ),
    );
}

/**
 *  Implements hook_permission().
 */
function main_supplier_permission()
{
    return array(
        'manage supplier notification emails' => array(
            'title' => t('Manage supplier notification emails'),
        ),
    );
}

/**
 * Implements hook_mail().
 */
function main_supplier_mail($key, &$message, $params)
{
    global $base_url;
    global $base_path;

    switch ($key) {
        case 'profile_submitted':
            $message['subject'] = variable_get('main_supplier_profile_submitted_mail_subject', '');
            $message['body'][] = variable_get('main_supplier_profile_submitted_mail_body', '');
            break;
        case 'profile_awaiting_validation':
            $message['subject'] = variable_get('main_supplier_profile_awaiting_validation_mail_subject', '');
            $message['body'][] = variable_get('main_supplier_profile_awaiting_validation_mail_body', '');
            break;
        case 'profile_edited':
            $message['subject'] = variable_get('main_supplier_profile_edited_mail_subject', '');

            $body = variable_get('main_supplier_profile_edited_mail_body', '');
            $link = $base_url . $base_path . drupal_get_path_alias('node/' . $params['profile_nid']);
            $body = str_replace('[link]', $link, $body);
            $message['body'][] = $body;
            break;
    }
}

/**
 * Implements hook_countries_alter().
 * Removes non UN countries.
 */
function main_supplier_countries_alter(&$countries)
{
    unset($countries['AI']);
    unset($countries['AQ']);
    unset($countries['AN']);
    unset($countries['AW']);

    unset($countries['BM']);
    unset($countries['BN']);
    unset($countries['BI']);

    unset($countries['CW']);

    unset($countries['GI']);
    unset($countries['GL']);
    unset($countries['GP']);
    unset($countries['GU']);
    unset($countries['GG']);
    unset($countries['GF']);
    unset($countries['GS']);

    unset($countries['HK']);

    unset($countries['JE']);

    unset($countries['KP']);

    unset($countries['LA']);

    unset($countries['MO']);
    unset($countries['MQ']);
    unset($countries['MS']);

    unset($countries['NR']);
    unset($countries['NU']);
    unset($countries['NC']);

    unset($countries['BQ']);
    unset($countries['PN']);
    unset($countries['PF']);
    unset($countries['PR']);

    unset($countries['RE']);

    unset($countries['EH']);
    unset($countries['BL']);
    unset($countries['KN']);
    unset($countries['SX']);
    unset($countries['MF']);
    unset($countries['PM']);
    unset($countries['SH']);
    unset($countries['AS']);
    unset($countries['SJ']);

    unset($countries['TW']);
    unset($countries['TF']);
    unset($countries['IO']);
    unset($countries['PS']);
    unset($countries['TK']);
    unset($countries['TK']);

    unset($countries['VA']);

    unset($countries['WF']);

    unset($countries['BV']);
    unset($countries['CX']);
    unset($countries['IM']);
    unset($countries['NF']);
    unset($countries['AX']);
    unset($countries['KY']);
    unset($countries['CC']);
    unset($countries['CK']);
    unset($countries['FO']);
    unset($countries['HM']);
    unset($countries['FK']);
    unset($countries['MP']);
    unset($countries['UM']);
    unset($countries['TC']);
    unset($countries['VI']);
    unset($countries['VG']);

    $countries['BN'] = 'Brunei';
    $countries['BI'] = 'Burundi';
    $countries['NR'] = 'Nauru';
    $countries['LA'] = 'Laos';
    $countries['KN'] = 'Saint Kitts et Nevis';

    asort($countries);

    //put A.R.E.N.A countries on top
    $arena_countries = array();
    foreach (array('AT', 'FR', 'DE', 'IT', 'PL', 'PT', 'RO', 'RU', 'CH', 'TH') as $code) {
        $arena_countries[$code] = $countries[$code];
        unset($countries[$code]);
    }

    $countries = array_merge($arena_countries, $countries);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function main_supplier_form_supplier_profile_node_form_alter(&$form, &$form_state, $form_id)
{
    field_group_hide_field_groups($form, array('group_access_none'));
}

/**
 * Implements hook_node_presave().
 */
function main_supplier_node_presave($node)
{
    if ($node->type == 'supplier_profile') {
        $result = db_query('SELECT field_key_figures_id FROM {field_data_field_key_figures} ORDER BY field_key_figures_id DESC LIMIT 1');
        foreach ($result as $row) {
            $starter_id = $row->field_key_figures_id + 1;
        }

        for ($i = 0; $i < 4; $i++) {
            if (!isset($node->field_key_figures[LANGUAGE_NONE][$i])) {
                $node->field_key_figures[LANGUAGE_NONE][$i] = array(
                    'id' => $starter_id,
                );
            }
            $starter_id++;
        }
        // first key figure
        $isFirstEmpty = TRUE;
        if (isset($node->field_additional_information[LANGUAGE_NONE][0]['value'])) {
            $value = json_decode($node->field_additional_information[LANGUAGE_NONE][0]['value'], TRUE);
            $year = date('Y');
            $year = (int)$year;
            $last_year = $year - 1;
            if (!empty($value['year-' . $last_year]['year']['total_turnover'])) {
                $figure_value = $value['year-' . $last_year]['year']['total_turnover'];
                $currency = MainSupplierAPI::getCurrency($node, TRUE);
                $currency = ($currency !== NULL) ? $currency : '';
                $million = pow(10, 6);
                if ($figure_value >= $million) {
                    $figure_value = round($figure_value / $million);
                    $currency = 'M' . $currency;
                }

                $node->field_key_figures[LANGUAGE_NONE][0]['field_figure_value'] = $figure_value;
                $node->field_key_figures[LANGUAGE_NONE][0]['field_figure_suffix_value'] = $currency;
                $node->field_key_figures[LANGUAGE_NONE][0]['field_figure_caption_value'] = $last_year . ' total turnover';
                $isFirstEmpty = FALSE;
            }
        }
        // second key figure
        $isSecondEmpty = TRUE;
        if (isset($node->field_market_client_information[LANGUAGE_NONE][0]['value'])) {
            $value = json_decode($node->field_market_client_information[LANGUAGE_NONE][0]['value'], TRUE);

            $turnover = 0;
            foreach ($value['country'] as $country) {
                if (isset($country['turnover_per_ally'])) {
                    foreach ($country['turnover_per_ally'] as $turnover_per_ally) {
                        $turnover += $turnover_per_ally['turnover'];
                    }
                }
            }

            $currency = MainSupplierAPI::getCurrency($node, TRUE);
            $million = pow(10, 6);
            if ($turnover >= $million) {
                $turnover = round($turnover / $million);
                $currency = 'M' . $currency;
            }

            $node->field_key_figures[LANGUAGE_NONE][1]['field_figure_value'] = $turnover;
            $node->field_key_figures[LANGUAGE_NONE][1]['field_figure_suffix_value'] = (($currency !== NULL) ? $currency : '');
            $node->field_key_figures[LANGUAGE_NONE][1]['field_figure_caption_value'] = 'Total turnover with A.R.E.N.A. members';
            $isSecondEmpty = FALSE;
        }
        // third key figure
        $isThirdEmpty = TRUE;
        if (isset($node->field_production_information[LANGUAGE_NONE][0]['value'])) {
            $value = json_decode($node->field_production_information[LANGUAGE_NONE][0]['value'], TRUE);
            if (!empty($value['production_sites_count'])) {
                $node->field_key_figures[LANGUAGE_NONE][2]['field_figure_value'] = $value['production_sites_count'];
                $node->field_key_figures[LANGUAGE_NONE][2]['field_figure_caption_value'] = 'Total of production units owned';
                $isThirdEmpty = FALSE;
            }
        }
        // fourth key figure
        $isFourthEmpty = TRUE;
        if (isset($node->field_production_information[LANGUAGE_NONE][0]['value'])) {
            $value = json_decode($node->field_production_information[LANGUAGE_NONE][0]['value'], TRUE);

            $employees = 0;
            if (!empty($value['human_resource'])) {
                foreach ($value['human_resource'] as $human_resource) {
                    $employees += !empty($human_resource['employees']) ? $human_resource['employees'] : 0;
                    $employees += !empty($human_resource['management']) ? $human_resource['management'] : 0;
                }
                if ($employees > 0) {
                    $node->field_key_figures[LANGUAGE_NONE][3]['field_figure_value'] = $employees;
                    $node->field_key_figures[LANGUAGE_NONE][3]['field_figure_caption_value'] = 'Total of employees';
                    $isFourthEmpty = FALSE;
                }
            }
        }
        //cleanup leftover key figures
        if ($isFirstEmpty) {
            unset($node->field_key_figures[LANGUAGE_NONE][0]['field_figure_value']);
            unset($node->field_key_figures[LANGUAGE_NONE][0]['field_figure_suffix_value']);
            unset($node->field_key_figures[LANGUAGE_NONE][0]['field_figure_caption_value']);
        }
        if ($isSecondEmpty) {
            unset($node->field_key_figures[LANGUAGE_NONE][1]['field_figure_value']);
            unset($node->field_key_figures[LANGUAGE_NONE][1]['field_figure_suffix_value']);
            unset($node->field_key_figures[LANGUAGE_NONE][1]['field_figure_caption_value']);
        }
        if ($isThirdEmpty) {
            unset($node->field_key_figures[LANGUAGE_NONE][2]['field_figure_value']);
            unset($node->field_key_figures[LANGUAGE_NONE][2]['field_figure_caption_value']);
        }
        if ($isFourthEmpty) {
            unset($node->field_key_figures[LANGUAGE_NONE][3]['field_figure_value']);
            unset($node->field_key_figures[LANGUAGE_NONE][3]['field_figure_caption_value']);
        }

        //handle workflow
        if (
            isset($node->original->field_moderation_status[LANGUAGE_NONE][0]['value'])
            && isset($node->field_moderation_status[LANGUAGE_NONE][0]['value'])
        ) {
            $original_moderation_status = $node->original->field_moderation_status[LANGUAGE_NONE][0]['value'];
            $moderation_status = $node->field_moderation_status[LANGUAGE_NONE][0]['value'];
            //if moderation status has changed
            if ($original_moderation_status != $moderation_status) {
                switch ($moderation_status) {
                    case MainSupplierAPI::MODERATION_STATUS_AWAITING_PROFILE_VALIDATION:
                        $account = user_load($node->uid);
                        //Temporarily disable user notification on account submitted (SIFTER #694)
                        //MainSupplierAPI::notify($account->mail, 'profile_submitted');
                        MainSupplierAPI::notify('admins', 'profile_awaiting_validation');
                        break;
                    case MainSupplierAPI::MODERATION_STATUS_PROFILE_VALIDATED:
                        //activate user account
                        $account = user_load($node->uid);
                        $account->status = 1;
                        user_save($account);

                        //publish profile
                        $node->status = 1;
                        break;
                }
            } else {
                $moderation_status = $node->field_moderation_status[LANGUAGE_NONE][0]['value'];
                if ($moderation_status == MainSupplierAPI::MODERATION_STATUS_PROFILE_VALIDATED) {
                    //MainSupplierAPI::notify('admins', 'profile_edited', array('profile_nid' => $node->nid));
                }
            }
        }
    }
}

/**
 * Implements hook_node_view().
 */
function main_supplier_node_view($node, $view_mode, $langcode)
{
    global $user;

    if ($node->type == 'supplier_profile' && $view_mode != 'teaser') {
        if (!MainSupplierAPI::isDeviceGranted()) {
            unset($node->content['field_general_information']);
            unset($node->content['field_additional_information']);
            unset($node->content['field_market_client_information']);
            unset($node->content['field_production_information']);
            unset($node->content['field_quality_control']);
            unset($node->content['field_media_picture']);
            unset($node->content['field_communication_documents']);
        }

        if (empty($node->field_establishment_date[LANGUAGE_NONE][0]['value'])) {
            unset($node->content['field_establishment_date']);
        }

        if (
            empty($node->field_key_figures[LANGUAGE_NONE][0]['field_figure'][LANGUAGE_NONE][0]['value']) &&
            empty($node->field_key_figures[LANGUAGE_NONE][1]['field_figure'][LANGUAGE_NONE][0]['value']) &&
            empty($node->field_key_figures[LANGUAGE_NONE][2]['field_figure'][LANGUAGE_NONE][0]['value']) &&
            empty($node->field_key_figures[LANGUAGE_NONE][3]['field_figure'][LANGUAGE_NONE][0]['value'])
        ) {
            unset($node->content['field_key_figures']);
        }

        if ($user->uid == $node->uid) {
            $node->content['edit_link'] = array(
                '#theme' => 'link',
                '#text' => 'Edit profile',
                '#path' => 'supplier',
                '#options' => array(
                    'attributes' => array('class' => 'arrow'),
                    'html' => FALSE,
                ),
            );
            $node->content['#groups']['group_main_header']->children[] = 'edit_link';
            $node->content['#group_children']['edit_link'] = 'group_main_header';
        }

        //add generated information
        $form = drupal_get_form('_main_supplier_general_information_form');
        $type_of_business = '';
        if (isset($node->field_general_information[LANGUAGE_NONE][0]['value'])) {
            $general_information = json_decode($node->field_general_information[LANGUAGE_NONE][0]['value'], TRUE);
            if (!empty($general_information['type_of_business']) && !empty($form['fieldset_1']['type_of_business'])) {
                if (!is_array($general_information['type_of_business'])) {
                    $general_information['type_of_business'] = array($general_information['type_of_business']);
                }
                foreach ($general_information['type_of_business'] as $key) {
                    $type_of_business .= $form['fieldset_1']['type_of_business']['#options'][$key];
                    $type_of_business .= ', ';
                }
                $type_of_business = substr($type_of_business, 0, -2);
            }
        }
        $brands = '';
        if (isset($node->field_additional_information[LANGUAGE_NONE][0]['value'])) {
            $additional_information = json_decode($node->field_additional_information[LANGUAGE_NONE][0]['value'], TRUE);
            if (isset($additional_information['brand'])) {
                foreach ($additional_information['brand'] as $brand) {
                    $brands .= $brand['brand_name'];
                    $brands .= '<br/>';
                }
            }
        }
        $business = '<div class="business"><h2>Business</h2>';
        if (!empty($type_of_business)) {
            $business .= '<h3>Type of business</h3>';
            $business .= '<p>' . $type_of_business . '</p>';
        }
        if (!empty($brands)) {
            $business .= '<h3>Brands</h3>';
            $business .= '<p>' . $brands . '</p>';
        }
        $business .= '</div>';

        $certifications = '<div class="certifications">';
        $form = drupal_get_form('_main_supplier_quality_control_form');
        if (isset($node->field_quality_control[LANGUAGE_NONE][0]['value'])) {
            $certifications .= '<h2>Certifications</h2>';
            $quality_control = json_decode($node->field_quality_control[LANGUAGE_NONE][0]['value'], TRUE);
            if (isset($quality_control['process_certification']) && !empty($quality_control['process_certification']['elements'])) {
                $quality_control['process_certification']['elements'] = array_filter($quality_control['process_certification']['elements']);
                if (count($quality_control['process_certification']['elements']) > 0) {
                    $certifications .= '<h3>' . $form['process_certification']['#title'] . '</h3>';
                    foreach ($quality_control['process_certification']['elements'] as $key2 => $value2) {
                        if ($value2 !== 0) {
                            $certifications .= '<span class="certification_label">' . strtoupper($value2) . '</span>';
                            $certifications .= ', ';
                        }
                    }
                    $certifications = substr($certifications, 0, -2);
                }
            }
            if (isset($quality_control['product_certificate']) && !empty($quality_control['product_certificate']['elements'])) {
                $quality_control['product_certificate']['elements'] = array_filter($quality_control['product_certificate']['elements']);
                if (count($quality_control['product_certificate']['elements']) > 0) {
                    $certifications .= '<h3>' . $form['product_certificate']['#title'] . '</h3>';
                    foreach ($quality_control['product_certificate']['elements'] as $key2 => $value2) {
                        if ($value2 !== 0) {
                            $certifications .= '<span class="certification_label">' . strtoupper($value2) . '</span>';
                            $certifications .= ', ';
                        }
                    }
                    $certifications = substr($certifications, 0, -2);
                }
            }
            if (isset($quality_control['quality_label']) && !empty($quality_control['quality_label']['elements'])) {
                $quality_control['quality_label']['elements'] = array_filter($quality_control['quality_label']['elements']);
                if (count($quality_control['quality_label']['elements']) > 0) {
                    $certifications .= '<h3>' . $form['quality_label']['#title'] . '</h3>';
                    foreach ($quality_control['quality_label']['elements'] as $key2 => $value2) {
                        if ($value2 !== 0) {
                            $certifications .= '<span class="certification_label">' . strtoupper($value2) . '</span>';
                            $certifications .= ', ';
                        }
                    }
                    $certifications = substr($certifications, 0, -2);
                }
            }
        }
        $certifications .= '</div>';

        $product = '';
        if (isset($node->field_market_client_information[LANGUAGE_NONE][0]['value'])) {
            $market_client_information = json_decode($node->field_market_client_information[LANGUAGE_NONE][0]['value'], TRUE);
            if (!empty($market_client_information['product_category'])) {
                $product = '<div class="row">';
                $product .= '<div class="table-wrapper col-md-12">';
                $product .= '<div class="table-title">Main product subfamilies turnover</div>';
                $product .= '<table class="top-header"><thead><tr><td>Product subfamily</td><td>Turnover percentage</td><td>Global turnover</td></tr></thead><tbody>';

                for ($i = 1; $i <= count($market_client_information['product_category']) && $i <= 4; $i++) {
                    if (isset($market_client_information['product_category'][$i])) {
                        $product .= '<tr>';
                        if (isset($market_client_information['product_category'][$i]['product_options'])) {
                            $tid = $market_client_information['product_category'][$i]['product_options'];
                            $term = taxonomy_term_load($tid);
                            $product .= '<td>' . $term->name . '</td>';
                        } else {
                            $product .= '<td></td>';
                        }
                        if (isset($market_client_information['product_category'][$i]['turnover_percentage'])) {
                            $product .= '<td>' . $market_client_information['product_category'][$i]['turnover_percentage'] . '</td>';
                        } else {
                            $product .= '<td></td>';
                        }
                        if (isset($market_client_information['product_category'][$i]['turnover'])) {
                            $product .= '<td>' . $market_client_information['product_category'][$i]['turnover'] . '</td>';
                        } else {
                            $product .= '<td></td>';
                        }
                        $product .= '</tr>';
                    }
                }
                $product .= '</tbody></table></div></div>';
            }
        }

        $production = '';
        if (isset($node->field_production_information[LANGUAGE_NONE][0]['value'])) {
            $production_information = json_decode($node->field_production_information[LANGUAGE_NONE][0]['value'], TRUE);
            if (!empty($production_information['factory_detail'])) {
                $production = '<div class="row">';
                $production .= '<div class="table-wrapper col-md-12">';
                $production .= '<div class="table-title">Main production sites</div>';
                $production .= '<table class="top-header"><thead><tr><td>Factory name</td><td>Country</td><td>Main product subfamily produced</td><td>Last year turnover</td></tr></thead><tbody>';

                for ($i = 1; $i <= count($production_information['factory_detail']) && $i <= 4; $i++) {
                    if (isset($production_information['factory_detail'][$i])) {
                        $production .= '<tr>';
                        if (isset($production_information['factory_detail'][$i]['factory_name'])) {
                            $production .= '<td>' . $production_information['factory_detail'][$i]['factory_name'] . '</td>';
                        } else {
                            $production .= '<td></td>';
                        }
                        if (isset($production_information['factory_detail'][$i]['country'])) {
                            $countries = country_get_list();
                            $production .= '<td>' . $countries[$production_information['factory_detail'][$i]['country']] . '</td>';
                        } else {
                            $production .= '<td></td>';
                        }
                        if (isset($production_information['factory_detail'][$i]['product_options'])) {
                            $tid = $production_information['factory_detail'][$i]['product_options'];
                            $term = taxonomy_term_load($tid);
                            $production .= '<td>' . $term->name . '</td>';
                        } else {
                            $production .= '<td></td>';
                        }
                        if (isset($production_information['factory_detail'][$i]['last_year_turnover'])) {
                            $production .= '<td>' . $production_information['factory_detail'][$i]['last_year_turnover'] . '</td>';
                        } else {
                            $production .= '<td></td>';
                        }
                        $production .= '</tr>';
                    }
                }
                $production .= '</tbody></table></div></div>';
            }
        }

        $node->content['business'] = array('#markup' => $business);
        $node->content['certifications'] = array('#markup' => $certifications);
        $node->content['product'] = array('#markup' => $product);
        $node->content['production'] = array('#markup' => $production);
        $node->content['#groups']['group_other_information']->children[] = 'business';
        $node->content['#group_children']['business'] = 'group_other_information';
        $node->content['#groups']['group_other_information']->children[] = 'certifications';
        $node->content['#group_children']['certifications'] = 'group_other_information';
        $node->content['#groups']['group_other_information']->children[] = 'product';
        $node->content['#group_children']['product'] = 'group_other_information';
        $node->content['#groups']['group_other_information']->children[] = 'production';
        $node->content['#group_children']['production'] = 'group_other_information';
    }
}


/**
 *  Implements hook_field_formatter_info().
 */
function main_supplier_field_formatter_info()
{
    return array(
        'country_name_from_code' => array(
            'label' => t('Country name'),
            'field types' => array('text'),
        ),
    );
}

/**
 *  Implements hook_field_formatter_view().
 */
function main_supplier_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
{
    global $language;
    global $user;

    $element = array();
    $settings = $display['settings'];

    switch ($display['type']) {
        case 'country_name_from_code':
            $countries = country_get_list();
            foreach ($items as $delta => $item) {
                $element[$delta] = array(
                    '#markup' => $countries[$item['value']],
                );
            }
            break;
    }
    return $element;
}

function _main_supplier_has_enabled_profile()
{
    global $user;
    if ($user->uid) {
        $profile_nid = MainSupplierAPI::getUserProfileNid($user->uid);
        return MainSupplierAPI::isProfileValidated($profile_nid);
    }
    return FALSE;
}
