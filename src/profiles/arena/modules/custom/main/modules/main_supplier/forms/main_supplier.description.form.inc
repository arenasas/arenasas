<?php

function _main_supplier_description_form($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $is_validated = MainSupplierAPI::isProfileValidated($nid);
    $profile = node_load($nid);

    $ajax = FALSE;
    if (isset($form_state['storage']['key_contact_person_count'])) {
        if (isset($form_state['triggering_element']['#parents'][0])) {
            $ajax = TRUE;
            if ($form_state['triggering_element']['#parents'][0] == 'add_person') {
                $form_state['storage']['key_contact_person_count']++;
            } elseif ($form_state['triggering_element']['#parents'][0] == 'remove_person') {
                $form_state['storage']['key_contact_person_count']--;

            }
        }
    } elseif (isset($profile->field_key_contact_person[LANGUAGE_NONE])) {
        $form_state['storage']['key_contact_person_count'] = count($profile->field_key_contact_person[LANGUAGE_NONE]);
    } else {
        $form_state['storage']['key_contact_person_count'] = 1;
    }

    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#title' => 'Identity',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['fieldset_1']['company_name'] = array(
        '#type' => 'textfield',
        '#title' => 'Company name',
        '#size' => 60,
        '#maxlength' => 128,
        '#default_value' => isset($profile->field_company_name[LANGUAGE_NONE][0]['value']) ? $profile->field_company_name[LANGUAGE_NONE][0]['value'] : '',
        '#required' => TRUE,
    );

    if ($is_validated) {
        $form['fieldset_1']['company_logo'] = array(
            '#type' => 'file',
            '#title' => 'Company logo',
            '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
        );

        $form['fieldset_1']['description'] = array(
            '#type' => 'textarea',
            '#title' => 'Description',
            '#description' => 'Please briefly describe the company. Limited to 1000 characters.',
            '#rows' => 10,
            '#default_value' => isset($profile->field_description[LANGUAGE_NONE][0]['value']) ? $profile->field_description[LANGUAGE_NONE][0]['value'] : '',
        );
        $form['fieldset_1']['establishment_date'] = array(
            '#type' => 'textfield',
            '#title' => 'Establishment date',
            '#default_value' => isset($profile->field_establishment_date[LANGUAGE_NONE][0]['value']) ? $profile->field_establishment_date[LANGUAGE_NONE][0]['value'] : '',
        );
    }

    $form['fieldset_1']['address'] = array(
        '#type' => 'textfield',
        '#title' => 'Address',
        '#size' => 60,
        '#maxlength' => 128,
        '#default_value' => isset($profile->field_address[LANGUAGE_NONE][0]['value']) ? $profile->field_address[LANGUAGE_NONE][0]['value'] : '',
        '#required' => TRUE,
    );
    $form['fieldset_1']['country'] = array(
        '#type' => 'select',
        '#title' => 'Country',
        '#description' => 'The countries on the top of the list correspond to our allies\' markets',
        '#options' => country_get_list(),
        '#empty_value' => '',
        '#default_value' => isset($profile->field_country[LANGUAGE_NONE][0]['value']) ? $profile->field_country[LANGUAGE_NONE][0]['value'] : '',
        '#required' => TRUE,
    );
    $form['fieldset_1']['website'] = array(
        '#type' => 'textfield',
        '#title' => 'Website',
        '#default_value' => isset($profile->field_website[LANGUAGE_NONE][0]['value']) ? $profile->field_website[LANGUAGE_NONE][0]['value'] : 'http://',
        '#required' => TRUE,
    );
    $form['fieldset_1']['phone'] = array(
        '#type' => 'textfield',
        '#title' => 'Phone',
        '#default_value' => isset($profile->field_phone[LANGUAGE_NONE][0]['value']) ? $profile->field_phone[LANGUAGE_NONE][0]['value'] : '+',
        '#required' => TRUE,
    );
    $form['fieldset_1']['fax'] = array(
        '#type' => 'textfield',
        '#title' => 'Fax',
        '#default_value' => isset($profile->field_fax[LANGUAGE_NONE][0]['value']) ? $profile->field_fax[LANGUAGE_NONE][0]['value'] : '+',
        '#required' => TRUE,
    );

    $form['fieldset_2'] = array(
        '#type' => 'fieldset',
        '#title' => 'Key contact persons',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        'key_contact_person' => array(
            '#type' => 'container',
            '#tree' => TRUE,
            '#prefix' => '<div id="key-contact-person"><span class="panel-subtitle">Key contact persons will be referenced contacts for A.R.E.N.A. team.</span>',
            '#suffix' => '</div>',
            '#attributes' => array(),
        ),
        'add_person' => array(
            '#type' => 'button',
            '#value' => 'Add key contact person',
            '#href' => '',
            '#limit_validation_errors' => array(),
            '#ajax' => array(
                'callback' => '_main_supplier_description_form_ajax_add_key_contact_person',
                'wrapper' => 'key-contact-person',
            ),
        ),
        'remove_person' => array(
            '#type' => 'button',
            '#value' => 'Remove key contact person',
            '#href' => '',
            '#limit_validation_errors' => array(),
            '#ajax' => array(
                'callback' => '_main_supplier_description_form_ajax_remove_key_contact_person',
                'wrapper' => 'key-contact-person',
            ),
        ),
    );

    $person_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'person_title' => array(
            '#type' => 'select',
            '#title' => 'Title',
            '#empty_value' => '',
            '#required' => TRUE,
            '#options' => array(
                0 => 'Mr',
                1 => 'Ms'
            ),
        ),
        'last_name' => array(
            '#type' => 'textfield',
            '#title' => 'Last name',
        ),
        'first_name' => array(
            '#type' => 'textfield',
            '#title' => 'First name',
        ),
        'position' => array(
            '#type' => 'textfield',
            '#title' => 'Position',
        ),
        'email' => array(
            '#type' => 'textfield',
            '#title' => 'Email',
        ),
        'phone' => array(
            '#type' => 'textfield',
            '#title' => 'Phone',
            '#default_value' => '+',
        ),
    );
    for ($i = 1; $i <= $form_state['storage']['key_contact_person_count']; $i++) {
        $tmp_person_form = $person_form;
        if ($ajax === FALSE) {
            $existing_person = isset($profile->field_key_contact_person[LANGUAGE_NONE][$i - 1]) ? $profile->field_key_contact_person[LANGUAGE_NONE][$i - 1] : NULL;
            if ($existing_person != NULL) {
                $tmp_person_form['person_title']['#default_value'] =
                    isset($existing_person['field_person_title'][LANGUAGE_NONE][0]['value']) ? $existing_person['field_person_title'][LANGUAGE_NONE][0]['value'] : '';
                $tmp_person_form['last_name']['#default_value'] =
                    isset($existing_person['field_last_name'][LANGUAGE_NONE][0]['value']) ? $existing_person['field_last_name'][LANGUAGE_NONE][0]['value'] : '';
                $tmp_person_form['first_name']['#default_value'] =
                    isset($existing_person['field_first_name'][LANGUAGE_NONE][0]['value']) ? $existing_person['field_first_name'][LANGUAGE_NONE][0]['value'] : '';
                $tmp_person_form['position']['#default_value'] =
                    isset($existing_person['field_position'][LANGUAGE_NONE][0]['value']) ? $existing_person['field_position'][LANGUAGE_NONE][0]['value'] : '';
                $tmp_person_form['email']['#default_value'] =
                    isset($existing_person['field_email'][LANGUAGE_NONE][0]['value']) ? $existing_person['field_email'][LANGUAGE_NONE][0]['value'] : '';
                $tmp_person_form['phone']['#default_value'] =
                    isset($existing_person['field_phone'][LANGUAGE_NONE][0]['value']) ? $existing_person['field_phone'][LANGUAGE_NONE][0]['value'] : '';
            }
        }
        // First contact person is required
        if ($i == 1) {
            $tmp_person_form['person_title']['#required'] = TRUE;
            $tmp_person_form['last_name']['#required'] = TRUE;
            $tmp_person_form['first_name']['#required'] = TRUE;
            $tmp_person_form['position']['#required'] = TRUE;
            $tmp_person_form['email']['#required'] = TRUE;
            $tmp_person_form['phone']['#required'] = TRUE;
        }
        $form['fieldset_2']['key_contact_person'][] = $tmp_person_form;
    }

    return _main_supplier_formalize($form);
}

//validate
function _main_supplier_description_form_validate($form, &$form_state)
{
    $values = $form_state['values'];

    // Regexp for check phone and fax number
    $regexp_phone = '/^\+[\d \-\(\)]{6,}$/';

    if (isset($values['description']) && strlen($values['description']) > 1000) {
        form_set_error('description', 'The description should be no longer than 1000 characters.');
    }

    // Check establishment date >= 1800 and <= now
    if (isset($values['establishment_date'])) {
        $establishment_date = $values['establishment_date'];
        $min_date = 1800;
        if ($establishment_date !== '' &&
            (!is_numeric($establishment_date) || intval($establishment_date) != $establishment_date ||
                $establishment_date < $min_date || $establishment_date > ((integer)date('Y')))
        ) {
            form_set_error('establishment_date', t('Invalid establishment date, you need to give number between @min and @max.',
                array('@min' => $min_date, '@max' => date('Y'))));
        }
    }

    // Check phone number and fax number
    $phone = $values['phone'];
    if (!empty($phone) && !preg_match($regexp_phone, $phone)) {
        form_set_error('phone', t('Invalid phone number, you need to give international phone number like "+331234567".'));
    }
    $fax = $values['fax'];
    if (!empty($fax) && !preg_match($regexp_phone, $fax)) {
        form_set_error('fax', t('Invalid fax number, you need to give international fax number like "+331234567".'));
    }

    // Check website url
    $url = $values['website'];
    if (!empty($url) && !valid_url($url, TRUE)) {
        form_set_error('website', t('Invalid website url, you need to give a url like "http://www.alliance-arena.com".'));
    }

    // Fix if user remove all contact person form, and try to add one.
    if (!isset($form_state['triggering_element']['#parents'][0]) || !$form_state['triggering_element']['#parents'][0] == 'add_person') {
        // One or more contact person
        if (!isset($values['key_contact_person']) || sizeof($values['key_contact_person']) < 1) {
            form_set_error('key_contact_person', t('"@fieldname" field is required, you need to give one or more contact person.',
              array(
                '@fieldname' => $form['fieldset_2']['#title'],
              )
            ));
        }
    }

    // Check contact person
    if (isset($values['key_contact_person'])) {
        $key_contact_person = $values['key_contact_person'];
        foreach ($key_contact_person as $id => $contact_person) {
            $phone = $contact_person['phone'];
            if (!empty($phone) && !preg_match($regexp_phone, $phone)) {
                form_set_error('key_contact_person][' . $id . '][phone', t('Invalid phone number for contact person, you need to give international phone number like "+331234567".'));
            }
            $email = $contact_person['email'];
            if (!empty($email) && !valid_email_address($email)) {
                form_set_error('key_contact_person][' . $id . '][email', t('Invalid email for contact person.'));
            }
        }
    }

    $nid = MainSupplierAPI::getCurrentNid();

    // Check file
    $file = file_save_upload('company_logo', array(
        'file_validate_image_resolution' => array(0, '480x480'),
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('png gif jpg jpeg'),
    ));

    // If the file passed validation:
    if ($file) {
        if ($file = file_move($file, 'private://')) {
            $form_state['storage']['file'] = $file;
        } else {
            form_set_error('file', t('Failed to write the uploaded file to the site\'s file folder.'));
        }
    } elseif ($nid === FALSE) {
        form_set_error('file', t('No file was uploaded.'));
    }
}

//submit
function _main_supplier_description_form_submit($form, &$form_state)
{
    $values = $form_state['values'];

    $uid = MainSupplierAPI::getCurrentUid();
    $nid = MainSupplierAPI::getCurrentNid();
    if ($nid === 0) {
        $profile = MainSupplierAPI::createProfile($uid, $values['company_name']);
    } else {
        $profile = node_load($nid);
    }

    if (isset($form_state['storage']['file'])) {
        $file = $form_state['storage']['file'];
        unset($form_state['storage']['file']);
        $profile->field_company_logo[LANGUAGE_NONE][0] = (array)$file;
    }

    $profile->field_company_name[LANGUAGE_NONE][0]['value'] = $values['company_name'];
    $profile->field_description[LANGUAGE_NONE][0]['value'] = !empty($values['description']) ? $values['description'] : '';
    $profile->field_establishment_date[LANGUAGE_NONE][0]['value'] = !empty($values['establishment_date']) ? $values['establishment_date'] : 0;
    $profile->field_address[LANGUAGE_NONE][0]['value'] = $values['address'];
    $profile->field_country[LANGUAGE_NONE][0]['value'] = $values['country'];
    $profile->field_website[LANGUAGE_NONE][0]['value'] = $values['website'];
    $profile->field_phone[LANGUAGE_NONE][0]['value'] = $values['phone'];
    $profile->field_fax[LANGUAGE_NONE][0]['value'] = $values['fax'];

    $profile->field_key_contact_person[LANGUAGE_NONE] = array();
    if (isset($values['key_contact_person'])) {
        for ($i = 0; $i < count($values['key_contact_person']); $i++) {
            $profile->field_key_contact_person[LANGUAGE_NONE][$i] = array(
                'field_person_title' => array(LANGUAGE_NONE => array(0 => array('value' => $values['key_contact_person'][$i]['person_title']))),
                'field_last_name' => array(LANGUAGE_NONE => array(0 => array('value' => $values['key_contact_person'][$i]['last_name']))),
                'field_first_name' => array(LANGUAGE_NONE => array(0 => array('value' => $values['key_contact_person'][$i]['first_name']))),
                'field_position' => array(LANGUAGE_NONE => array(0 => array('value' => $values['key_contact_person'][$i]['position']))),
                'field_email' => array(LANGUAGE_NONE => array(0 => array('value' => $values['key_contact_person'][$i]['email']))),
                'field_phone' => array(LANGUAGE_NONE => array(0 => array('value' => $values['key_contact_person'][$i]['phone']))),
            );
        }
    }

    node_save($profile);

    MainSupplierAPI::setCurrentNid($profile->nid);

    drupal_set_message('Information has been saved.');
    drupal_goto('supplier/general-information');
}


/**********
 ** AJAX **
 **********/
function _main_supplier_description_form_ajax_add_key_contact_person($form, &$form_state)
{
    return $form['fieldset_2']['key_contact_person'];
}

function _main_supplier_description_form_ajax_remove_key_contact_person($form, &$form_state)
{
    return $form['fieldset_2']['key_contact_person'];
}
