<div class="substep-anchors <?php echo $process_type ?> step-<?php echo $step_index ?>">
    <div class="title">
        <?php echo $title ?>
    </div>
    <?php foreach ($anchors as $anchor) : ?>
        <div class="anchor">
            <a href="<?php echo $anchor['path'] ?>"><?php echo $anchor['title'] ?></a>
        </div>
    <?php endforeach; ?>
</div>
