<div class="process-anchors">
    <?php for($i = 0; $i < count($anchors); $i++) : ?>
        <div class="anchor <?php echo $process_type ?> step-<?php echo $i +1 ?>">
            <a href="<?php echo $anchors[$i]['path'] ?>"><?php echo $anchors[$i]['title'] ?></a>
        </div>
    <?php endfor; ?>
</div>
