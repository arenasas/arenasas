<?php

function _my_space_admin_form() {
  $form = array();

  $form['main_private_international_process_text'] = array(
    '#title' => 'International process text',
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => variable_get('main_private_international_process_text', ''),
  );

  $form['main_private_international_process_entity_reference'] = array(
      '#title' => 'International process entity reference',
      '#type' => 'entityreference',
      '#required' => TRUE,
      '#era_entity_type' => 'node',
      '#era_bundles' => array('process'),
      '#era_cardinality' => 1,
      '#era_query_settings' => array(
          'limit' => 15,
      ),
      '#default_value' => variable_get('main_private_international_process_entity_reference', ''),
      '#access' => user_access('administer content'),
  );

  $form['main_private_import_process_text'] = array(
    '#title' => 'Import process text',
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => variable_get('main_private_import_process_text', ''),
  );

  $form['main_private_import_process_entity_reference'] = array(
      '#title' => 'Import process entity reference',
      '#type' => 'entityreference',
      '#required' => TRUE,
      '#era_entity_type' => 'node',
      '#era_bundles' => array('process'),
      '#era_cardinality' => 1,
      '#era_query_settings' => array(
          'limit' => 15,
      ),
      '#default_value' => variable_get('main_private_import_process_entity_reference', ''),
      '#access' => user_access('administer content'),
  );

  $form['main_private_documentation_text'] = array(
    '#title' => 'Documentation text',
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => variable_get('main_private_documentation_text', ''),
  );

  $form['main_private_product_catalog_text'] = array(
    '#title' => 'Product catalog text',
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => variable_get('main_private_product_catalog_text', ''),
  );

  $form['main_private_suppliers_directory_text'] = array(
    '#title' => 'Suppliers directory text',
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => variable_get('main_private_suppliers_directory_text', ''),
  );

  return system_settings_form($form);
}
