(function ($) {
    Drupal.behaviors.treeDropDown = {
        attach: function() {
            var $block = jQuery('#block-main-private-main-private-contextual');
            $block.find('.subbranch-container').each(function(index, element) {
                var $element = jQuery(element);
                $element.find('.subbranch-title').click(function() {
                    $element.toggleClass('open');
                    $element.find('.subsubbranch-container').slideToggle(200);
                });
            });
        }
    };
})(jQuery);
