<?php

function _main_private_space_403() {
  return 'You are not authorized to access this page, you are only authorized to access "Product catalog".';
}

function _main_private_space()
{
    global $user;
    $user = user_load($user->uid);

    $welcome_message = 'Welcome';
    if (isset($user->field_first_name[LANGUAGE_NONE][0]['value'])) {
        $welcome_message .= ' ' . $user->field_first_name[LANGUAGE_NONE][0]['value'];
    }
    drupal_set_title($welcome_message);

    $international = variable_get('main_private_international_process_entity_reference', '');
    $path_elements = MainPrivateController::pathAuto($international['entity_id']);
    $big_push_link1 = array(
        '#theme' => 'link',
        '#text' => 'Read more',
        '#path' => $path_elements['path'],
        '#options' => array(
            'attributes' => array(
                'class' => 'arrow'
            ),
            'html' => FALSE,
        ),
    );

    $import = variable_get('main_private_import_process_entity_reference', '');
    $path_elements = MainPrivateController::pathAuto($import['entity_id']);
    $big_push_link2 = array(
        '#theme' => 'link',
        '#text' => 'Read more',
        '#path' => $path_elements['path'],
        '#options' => array(
            'attributes' => array(
                'class' => 'arrow'
            ),
            'html' => FALSE,
        ),
    );

    $suppliers_link = array(
        '#theme' => 'link',
        '#text' => 'Read more',
        '#path' => 'my-space/suppliers-directory',
        '#options' => array(
            'attributes' => array(
                'class' => 'arrow'
            ),
            'html' => FALSE,
        ),
    );

    $catalog_link = array(
        '#theme' => 'link',
        '#text' => 'Read more',
        '#path' => 'my-space/product-catalog',
        '#options' => array(
            'attributes' => array(
                'class' => 'arrow'
            ),
            'html' => FALSE,
        ),
    );

    $documentation_link = array(
        '#theme' => 'link',
        '#text' => 'Read more',
        '#path' => 'my-space/documentation',
        '#options' => array(
            'attributes' => array(
                'class' => 'arrow'
            ),
            'html' => FALSE,
        ),
    );

    return array(
        array(
            '#theme' => 'link',
            '#text' => 'Edit account',
            '#path' => 'user/' . $user->uid . '/edit',
            '#options' => array(
                'attributes' => array('class' => 'arrow edit-account'),
                'html' => FALSE,
            ),
        ),
        array(
            '#theme' => 'main_private_push',
            '#title' => 'International Process',
            '#text' => variable_get('main_private_international_process_text', ''),
            '#link' => drupal_render($big_push_link1),
            '#class' => 'process_international process',
        ),
        array(
            '#theme' => 'main_private_push',
            '#title' => 'Import Process',
            '#text' => variable_get('main_private_import_process_text', ''),
            '#link' => drupal_render($big_push_link2),
            '#class' => 'process_import process',
        ),
        array(
            '#theme' => 'main_private_push',
            '#title' => 'Documentation',
            '#text' => variable_get('main_private_documentation_text', ''),
            '#link' => drupal_render($documentation_link),
            '#class' => 'suppliers',
        ),
        array(
            '#theme' => 'main_private_push',
            '#title' => 'Product catalog',
            '#text' => variable_get('main_private_product_catalog_text', ''),
            '#link' => drupal_render($catalog_link),
            '#class' => 'catalog'
        ),
        array(
            '#theme' => 'main_private_push',
            '#title' => 'Suppliers directory',
            '#text' => variable_get('main_private_suppliers_directory_text',
                ''),
            '#link' => drupal_render($suppliers_link),
            '#class' => 'documentation'
        ),
    );
}

function _main_private_steps_callback($node)
{
    if ($node->type != 'process') {
        return drupal_not_found();
    }

    $view = node_view($node, 'teaser');
    return drupal_render($view);
}
