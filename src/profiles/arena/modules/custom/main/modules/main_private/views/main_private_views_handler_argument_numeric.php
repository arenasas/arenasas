<?php

class main_private_views_handler_argument_numeric extends views_handler_argument_numeric
{
    function query($group_by = FALSE)
    {
        $this->ensure_my_table();

        $vocabulary = taxonomy_vocabulary_machine_name_load('document_category');
        $tree = taxonomy_get_tree($vocabulary->vid, $this->value[0]);
        foreach ($tree as $branch) {
            $this->value[] = $branch->tid;
        }
        $this->query->add_table('field_data_field_category');
        $this->query->add_where(0, 'field_data_field_category.field_category_target_id', $this->value, 'IN');
    }
}
