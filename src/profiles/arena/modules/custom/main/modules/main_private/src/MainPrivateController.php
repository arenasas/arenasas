<?php

class MainPrivateController
{
    public static function isMySpace()
    {
        $path = drupal_get_path_alias(current_path());
        if (strpos($path, 'my-space') === 0) {
            return true;
        }
        if (arg(0) == 'node' && is_numeric(arg(1)) && function_exists('node_load')) {
            $node = node_load(arg(1));
            if ($node->type == 'process' || $node->type == 'product') {
                return TRUE;
            }
        }

        return false;
    }

    public static function pathAuto(
        $process_nid,
        $step_index = NULL,
        $substep_nid = NULL
    ) {
        global $base_path;

        $fragment = '';
        if (isset($substep_nid)) {
            $path = $base_path . drupal_get_path_alias('node/' . $substep_nid);
        } elseif ($step_index) {
            $path = $base_path . drupal_get_path_alias('node/' . $process_nid) . '/steps';
            $fragment = 'step-' . $step_index;
        } else {
            $path = $base_path . drupal_get_path_alias('node/' . $process_nid);
        }

        return array(
            'path' => $path,
            'fragment' => $fragment,
        );
    }

    public static function getStepIndexFromProcess($step_nid, $process_nid)
    {
        $process = node_load($process_nid);
        foreach ($process->field_process_steps[LANGUAGE_NONE] as $key => $step) {
            if ($step['target_id'] == $step_nid) {
                return $key + 1;
            }
        }
    }

    //parent getters
    public static function getStepParentProcess($step_nid)
    {
        return self::getParentNode($step_nid, 'process', 'field_process_steps');
    }

    public static function getSubstepParentStep($substep_nid)
    {
        return self::getParentNode($substep_nid, 'steps', 'field_sub_steps');
    }

    public static function getSubsubstepParentSubstep($subsubstep_nid)
    {
        return self::getParentNode($subsubstep_nid, 'sub_steps',
            'field_sub_sub_steps');
    }

    private static function getParentNode(
        $child_nid,
        $parent_type,
        $parent_field_name
    ) {
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', $parent_type)
            ->fieldCondition($parent_field_name, 'target_id', $child_nid);
        $result = $query->execute();
        if (isset($result['node'])) {
            $keys = array_keys($result['node']);
            return $keys[0];
        }
        return false;
    }
}
