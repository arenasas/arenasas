<?php

class MainUser {
    /**
     * Check to see if a user has been assigned a certain role.
     *
     * @param $role
     *   The name of the role you're trying to find.
     * @param $account
     *   The user object for the user you're checking; defaults to the current user.
     * @return
     *   TRUE if the user object has the role, FALSE if it does not.
     */
    public static function hasRole($role, $account = NULL) {
        global $user;
        if ($account === NULL) {
            $account = $user;
        }

        if (is_array($account->roles) && in_array($role, array_values($account->roles))) {
            return TRUE;
        }

        return FALSE;
    }
}
