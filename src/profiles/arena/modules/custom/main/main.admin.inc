<?php

function _main_contact_admin_form($form, &$form_state)
{
    $form = array();

    if (isset($form_state['storage']['country_count'])) {
        if (isset($form_state['triggering_element']['#parents'][0])) {
            if ($form_state['triggering_element']['#parents'][0] == 'add_country') {
                $form_state['storage']['country_count']++;
            } elseif ($form_state['triggering_element']['#parents'][0] == 'remove_country') {
                $form_state['storage']['country_count'] = max($form_state['storage']['country_count'] - 1, 0);
            }
        }
    } else {
        $variable = variable_get('main_contact', '');
        $variable = json_decode($variable, TRUE);
        $form_state['storage']['country_count'] = count($variable['country']);
    }

    $form['fieldset_1']['country'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="country">',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_1']['add_country'] = array(
        '#type' => 'button',
        '#value' => 'Add country',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_contact_admin_form_ajax_add_country',
            'wrapper' => 'country',
        ),
    );
    $form['fieldset_1']['remove_country'] = array(
        '#type' => 'button',
        '#value' => 'Remove country',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_contact_admin_form_ajax_remove_country',
            'wrapper' => 'country',
        ),
    );

    $country_form = array(
        'country' => array(
            '#type' => 'select',
            '#title' => 'Country',
            '#options' => country_get_list(),
        ),
        'email' => array(
            '#type' => 'textfield',
            '#title' => 'Email address',
        ),
    );

    for ($i = 0; $i < $form_state['storage']['country_count']; $i++) {
        $country_form['country']['#default_value'] = isset($variable['country'][$i]['country']) ? $variable['country'][$i]['country'] : '';
        $country_form['email']['#default_value'] = isset($variable['country'][$i]['email']) ? $variable['country'][$i]['email'] : '';
        $form['fieldset_1']['country'][] = $country_form;
    }

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
    );
    return $form;
}

function _main_contact_admin_form_submit($form, &$form_state) {
    $values = $form_state['values'];
    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op', 'add_country', 'remove_country') as $value) {
        unset($values[$value]);
    }
    variable_set('main_contact', json_encode($values));
}

/**********
 ** AJAX **
 **********/
function _main_contact_admin_form_ajax_add_country($form, &$form_state)
{
    return $form['fieldset_1']['country'];
}

function _main_contact_admin_form_ajax_remove_country($form, &$form_state)
{
    return $form['fieldset_1']['country'];
}
