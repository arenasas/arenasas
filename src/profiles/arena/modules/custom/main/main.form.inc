<?php

/**
 * Format contact form
 *
 * @param type $form
 * @param type $form_state
 * @return string
 */
function _main_contact_form($form, $form_state)
{
    global $user;

    $variable = variable_get('main_contact', '');
    $variable = json_decode($variable, TRUE);
    $options = array();
    $countries = country_get_list();
    foreach ($variable['country'] as $country) {
        $code = $country['country'];
        $options[$code] = $countries[$code];
    }

    $form['email'] = array(
        '#type' => 'textfield',
        '#title' => t('Your email'),
        '#required' => TRUE,
        '#default_value' => isset($user->mail) ? $user->mail : '',
    );
    $form['recipient'] = array(
        '#type' => 'select',
        '#title' => t('Your recipient'),
        '#required' => TRUE,
        '#options' => $options,
    );
    $form['message'] = array(
        '#type' => 'textarea',
        '#required' => TRUE,
        '#title' => t('Your message'),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Send message'),
    );

    return $form;
}

/**
 * Contact form validation
 *
 * @param type $form
 * @param type $form_state
 */
function _main_contact_form_validate($form, $form_state)
{
    if (!valid_email_address($form_state['values']['email'])) {
        form_set_error('email', t('Your email address is invalid'));
    }
}

/**
 * Contact form submition
 *
 * @param type $form
 * @param type $form_state
 */
function _main_contact_form_submit($form, $form_state)
{
    $variable = variable_get('main_contact', '');
    $variable = json_decode($variable, TRUE);
    foreach ($variable['country'] as $country) {
        $code = $country['country'];
        if ($code == $form_state['values']['recipient']) {
            $to = $country['email'];
        }
    }

    $params = array(
        'from' => $form_state['values']['email'],
        'message' => $form_state['values']['message']
    );

    drupal_mail('main', 'contact', $to, 'en', $params);

    drupal_set_message(t('Your message has been sent'), 'status', false);
}
