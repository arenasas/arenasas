#!/bin/bash

### Make drush a variable to use the one shipped with the repository.
DRUSH=`which drush`

if [ ! ${DRUSH} ]; then
    echo "${DRUSH} drush is not available $(whoami)."
    echo "try to load drush from composer bin"
    export PATH="$HOME/.composer/vendor/bin:$PATH"
    DRUSH=`which drush`
    echo "Drush loaded from composer bin."
fi

if [ ! ${DRUSH} ]; then
    echo "${DRUSH} drush is definitevely not available $(whoami)."
    echo
    exit 1;
fi

# Default environment is prod.
ENV="prod";

# Default build mode. Can be install or update.
BUILD_MODE="update";

# Backup base before build.
BACKUP_BASE=0;

### Directory Profile name
PROFILE="arena"

# Default URI.
URI="http://fsp.alliance-arena.com/"

# /!\ This script needs the bc package installed on the server.
# Helper to let you run the install script from anywhere.
currentscriptpath () {
  SOURCE="${BASH_SOURCE[0]}"
  # resolve $SOURCE until the file is no longer a symlink
  while [ -h "$SOURCE" ]; do

    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
  done
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  echo $DIR
}



# Set the arguments value.
while [[ $1 ]]
do
  key="$1"
  if [ -z "$key" ]; then
    shift
  else
    case $key in
      -e | --env)
        case $2 in
          dev|recette|preprod|prod)
            ENV="$2"
            echo "[Environment built] $ENV"
            ;;
          *)
            echo "Unknown environment: $2. Please check your name."
            exit 1
        esac
        shift
        ;;
      -m|--mode)
        BUILD_MODE="$2"
        if [ $2 != "install" ] && [ $2 != "update" ] ; then
      echo "Invalid build mode."
      exit 1
    fi;
        echo "[Build mode] $2"
        shift
        ;;
      -b|--backup)
        BACKUP_BASE="$2"
        echo "[Generate a backup] $2"
        shift
        ;;
      -u|--uri)
        URI="$2"
        echo "[Environment URI] $2"
        shift
        ;;
      --) # End of all options
        shift
        ;;
      *) # No more options
        ;;
    esac
    shift
  fi
done

# Working directory.
RESULT=$(currentscriptpath)
WEBROOT="$RESULT/../htdocs"

# Make drush a variable to use the one shipped with the repository.
DRUSH="${DRUSH} --root=$WEBROOT --uri=${URI}"

# Actions to run before the main and shared deployment actions.
# It can be useful to backup, import databases or doing something similar.
predeploy_actions() {
  case $1 in
    dev)
      $DRUSH sql-drop -y;
      $DRUSH sqlc < "$WEBROOT/../dump_db_prod_latest.sql";
      ;;
    recette)
#      $DRUSH sql-drop -y;
#      $DRUSH sqlc < "$WEBROOT/../dump_db_prod_latest.sql";
      ;;
    preprod|prod)
        echo "Re export path for mysql"
        export PATH=$PATH:/opt/mysql-5.6.30/bin
      ;;
    *)
      echo "Unknown environment: $2. Please check your name."
      exit 1;
  esac
}

# Action to run after the main and shared deployment actions.
# It can be useful to enable specific modules for instance.
postdeploy_actions() {
  case $1 in
    dev)
      # Turn off the aggregation to avoid to turn crazy.
      $DRUSH vset preprocess_css 0;
      $DRUSH vset preprocess_js 0;
      # Enable UIs.
      $DRUSH en -y devel field_ui;
      # Enable proxy files
#      $DRUSH en -y stage_file_proxy;
#      $DRUSH vset stage_file_proxy_origin "$URI"
      # Connect.
#      $DRUSH uli

        echo 'Generate CSS'
        cd "$WEBROOT"/profiles/arena/themes/custom/arena
        npm install
        gulp sass:dev
      ;;
    recette|preprod|prod)
        echo 'Generate CSS'
        cd "$WEBROOT"/profiles/arena/themes/custom/arena
        npm install
        gulp sass:prod
      ;;
    *)
      echo "Unknown environment: $1. Please check your name."
      exit 1;
  esac
}


# Run the potential actions to do pre deployment.
predeploy_actions $ENV

if [ "$BACKUP_BASE" == "1" ] ; then
  # Store a security backup in case the update doesn't go right.
  DUMP_NAME="update-backup-script-$(date +%Y%m%d%H%M%S).sql";
  DUMP_PATH="$WEBROOT/../update_safe/"
  mkdir -p $DUMP_PATH
  DUMP_FULLPATH=$DUMP_PATH$DUMP_NAME
  $DRUSH sql-dump > $DUMP_FULLPATH;
  gzip $DUMP_FULLPATH
fi


echo 'Put the site offline for visitors.'
$DRUSH vset maintenance_mode 1;

echo 'Update contrib module'
cd $WEBROOT/profiles/$PROFILE
$DRUSH -y make --cache-duration-releasexml=300 --concurrency=8 --no-core --contrib-destination=. project.make .

echo 'Rebuild the registry in case some modules have been moved.'
$DRUSH registry-rebuild;
$DRUSH cc all

echo 'Run the database updates.'
$DRUSH -y updb

echo 'Clear the caches.'
$DRUSH cc all

echo 'Revert the features.'
$DRUSH -y fra

echo 'Clear the caches.'
$DRUSH cc all

echo 'Run the potential actions to do post deployment.'
postdeploy_actions $ENV

echo 'Put the site back online.'
$DRUSH vset maintenance_mode 0;
