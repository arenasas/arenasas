; Base Drupal
core = 7.x

; Internal API for Drush make
api = 2

; Modules
; Drush make allows a default sub directory for all contributed projects.
defaults[projects][subdir] = contrib

; Contrib modules
projects[admin_menu][version] = 3.0-rc5
projects[admin_theme][version] = 1.0
projects[admin_views][version] = 1.6
projects[admin_language][version] = 1.0-beta1
projects[auto_nodequeue][version] = 2.1
projects[backup_migrate][version] = 3.5
projects[better_exposed_filters][version] = 3.5
projects[better_formats][version] = 1.0-beta2
projects[captcha][version] = 1.5
projects[colorbox][version] = 2.10
projects[countries][version] = 2.3
projects[ctools][version] = 1.9
projects[date][version] = 2.9
projects[diff][version] = 3.2
projects[date][version] = 2.9
projects[devel][version] = 1.5
projects[email][version] = 1.3
projects[email_registration][version] = 1.3
projects[entity][version] = 1.9
projects[entity_translation][version] = 1.0-beta6
;projects[entity_translation][patch][] = https://www.drupal.org/files/issues/entity_translation-dont_list_untranslated_nodes-1452820-40.patch
projects[entityform][version] = 2.0-rc1
projects[entityqueue][version] = 1.1
projects[entityreference][version] = 1.5
projects[entityreference_autocomplete][version] = 1.11
projects[eu_cookie_compliance][version] = 1.25
projects[facetapi][version] = 1.5
projects[facetapi][patch][] = https://www.drupal.org/files/issues/toggle-facet-count-display-2194423-10.patch
projects[features][version] = 2.10
projects[features_extra][version] = 1.0
projects[features_extra][patch][] = https://www.drupal.org/files/issues/features_extra-add-date-format-support-1279928-49.patch
projects[field_formatter_settings][version] = 1.1
projects[field_group][version] = 1.5
projects[field_slideshow][version] = 1.82
projects[file_entity][version] = 2.0-beta2
projects[file_entity][patch][] = https://www.drupal.org/files/issues/fix_entity_api_create_access-2312603-8.patch
projects[fontawesome][version] = 2.5
projects[geolocation][version] = 1.6
projects[globalredirect][version] = 1.5
projects[google_analytics][version] = 2.4
projects[hierarchical_select][version] = 3.0-beta5
projects[i18n][version] = 1.13
projects[image_style_quality][version] = 1.5
projects[inline_entity_form][version] = 1.x-dev
projects[jquery_update][version] = 2.7
projects[l10n_update][version] = 2.0
projects[libraries][version] = 2.2
projects[link][version] = 1.4
projects[mailcontrol][version] = 1.0
projects[media][version] = 2.19
projects[media_oembed][version] = 2.6
projects[menu_attributes][version] = 1.0
projects[menu_block][version] = 2.7
projects[modal_forms][version] = 1.2
projects[mefibs][version] = 1.0-alpha1
projects[metatag][version] = 1.7
projects[migrate][version] = 2.8
projects[module_filter][version] = 2.x-dev
projects[mpac][version] = 1.2
projects[multifield][version] = 1.0-alpha1
projects[multifield][patch][] = https://www.drupal.org/files/issues/multifield-integrity-constraint-violation-2327317-40.patch
projects[navigation404][version] = 1.0
projects[node_view_permissions][version] = 1.5
projects[nodequeue][version] = 2.0
projects[password_policy][version] = 1.12
projects[pathauto][version] = 1.3
projects[picture][version] = 2.13
projects[publish_button][version] = 1.1
projects[r4032login][version] = 1.8
projects[role_delegation][version] = 1.1
projects[rules][version] = 2.9
projects[search_api][version] = 1.18
projects[search_api_db][version] = 1.5
projects[scheduler][version] = 1.x-dev
projects[subpathauto][version] = 1.3
projects[stringoverrides][version] = 1.8
projects[strongarm][version] = 2.0
projects[taxonomy_manager][version] = 1.0
projects[title][version] = 1.0-alpha9
projects[token][version] = 1.6
projects[transliteration][version] = 3.2
projects[user_expire][version] = 1.4
projects[variable][version] = 2.5
projects[viewfield][version] = 2.0
projects[views][version] = 3.20
projects[views_bulk_operations][version] = 3.3
projects[webform][version] = 4.12
projects[workbench][version] = 1.2
projects[wysiwyg][version] = 2.x-dev
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][url] = "http://git.drupal.org/project/wysiwyg.git"
projects[wysiwyg][download][revision] = "898d022cf7d0b6c6a6e7d813199d561b4ad39f8b"
projects[wysiwyg_filter][version] = 1.6-rc2


; Contrib themes
projects[adminimal_theme][version] = 1.24
projects[adminimal_admin_menu][version] = 1.7
projects[bootstrap][version] = 3.8

; Libraries
libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.5/ckeditor_4.5.5_standard.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

libraries[colorbox][download][type]= "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][destination] = "libraries"

libraries[jquery.cycle][download][type]= "get"
libraries[jquery.cycle][download][url] = "http://malsup.github.io/jquery.cycle.all.js"
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][destination] = "libraries"

libraries[fontawesome][download][type]= "get"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome/archive/v4.6.3.zip"
libraries[fontawesome][directory_name] = "fontawesome"
libraries[fontawesome][destination] = "libraries"
